import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { BottomHeaderComponent } from './nav/bottom-header/bottom-header.component';
import { TopHeaderComponent } from './nav/top-header/top-header.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { ProductInfoComponent } from './product/product-info/product-info.component';
import { RelatedProductComponent } from './product/related-product/related-product.component';
import { ProductImageComponent } from './product/product-image/product-image.component';

@NgModule({
  declarations: [
    AppComponent,
    BottomHeaderComponent,
    TopHeaderComponent,
    ProductDetailsComponent,
    ProductInfoComponent,
    RelatedProductComponent,
    ProductImageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
