import { Component, OnInit } from '@angular/core';
import { ProductService, Product } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  products : Product;

  constructor(private httpproduct: ProductService) { }

  ngOnInit(): void {
    this.httpproduct.getProduct().subscribe(
      (response:any) =>{this.products = response;}
     );
  }

}
