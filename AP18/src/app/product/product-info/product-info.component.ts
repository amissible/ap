import { Component, OnInit } from '@angular/core';
import { ProductService, Product } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  products : Product;

  constructor(private httpproduct: ProductService) { }

  ngOnInit(): void {
    this.httpproduct.getProduct().subscribe(
      response =>{this.products = response;}
     );
  }

}
