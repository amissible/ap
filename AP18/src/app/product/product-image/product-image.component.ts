import { Component, OnInit } from '@angular/core';
import { ProductService, Product } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['./product-image.component.css']
})
export class ProductImageComponent implements OnInit {

products : Product;

  constructor(private httpproduct: ProductService) { }

  ngOnInit(): void {
    this.httpproduct.getProduct().subscribe(
      response =>{this.products = response;}
     );
  }

}
