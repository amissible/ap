import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface Product {
    category : any;
    product_name: any;
    alias : any;
    info : any;
    image_path : any;
    attributes : any;
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor( private httpproduct: HttpClient ) { }

  getProduct() {
    return this.httpproduct.get<Product>('http://www.amissible.com:8082/db/product/blue-sanitizer-bracelet');
  }
  getQuickProduct() {
    return this.httpproduct.get<Product>('http://www.amissible.com:8082/static/quickview/grey-sanitizer-bracelet');
  }
}
