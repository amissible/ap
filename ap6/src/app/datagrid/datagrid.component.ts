import { Component, ViewChild, OnInit } from '@angular/core';
declare var $;

@Component({
  selector: 'app-datagrid',
  templateUrl: './datagrid.component.html',
  styleUrls: ['./datagrid.component.css']
})
export class DatagridComponent implements OnInit {
  @ViewChild('dataTable', { static: true }) table;
  dataTable: any;
  dtOptions: any = {};
  constructor() { }

  ngOnInit(): void {
    this.dtOptions = { 'pagingType' : 'full_numbers' };    
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }
}
