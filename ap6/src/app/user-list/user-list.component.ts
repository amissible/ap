import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientService } from '../service/http-clients.service';
declare var $;

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  
  @ViewChild('dataTable', { static: true }) table;
  dataTable: any;
  dtOptions: any = {};
  data:any

  constructor(private organi: HttpClientService){
    this.organi.getUser().subscribe(
      data=>{
        this.data = data
      }
    )
  }

  ngOnInit(): void {
    this.dtOptions = { 'pagingType' : 'full_numbers' };    
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);
  }
}
