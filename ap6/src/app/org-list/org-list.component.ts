import { Component, OnInit, ViewChild } from '@angular/core';
import { data } from 'jquery';
import { HttpClientService } from '../service/http-clients.service';
declare var $;

@Component({
  selector: 'app-org-list',
  templateUrl: './org-list.component.html',
  styleUrls: ['./org-list.component.css']
})
export class OrgListComponent implements OnInit {
 
  @ViewChild('dataTable', {static: true}) table;
  dataTable: any;
  dtOptions: any = {};
  data:any

  constructor(private organi: HttpClientService){
    
  }

  ngOnInit(): void {
    this.dtOptions = { 'pagingType' : 'full_numbers' };    
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable(this.dtOptions);

    this.organi.getOrg().subscribe(
      data=>{
        this.data = data
      }
    )
}
}