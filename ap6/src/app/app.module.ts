import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginCompComponent } from './login-comp/login-comp.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { DatagridComponent } from './datagrid/datagrid.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { OrgListComponent } from './org-list/org-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { ProductCatComponent } from './product-cat/product-cat.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginCompComponent,
    AboutusComponent,
    DatagridComponent,
    NavBarComponent,
    OrgListComponent,
    UserListComponent,
    ProductCatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    RouterModule.forRoot([
      {
        path: 'about',
        component : AboutusComponent
      },
      {
        path: 'datagrid',
        component : DatagridComponent
      },
      {
        path: 'org',
        component : OrgListComponent
      },
      {
        path: 'user',
        component : UserListComponent
      },
      {
        path : '',
        component: LoginCompComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
