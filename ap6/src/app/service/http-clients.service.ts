import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private http:HttpClient) { }

  getOrg(){
    let url = "http://localhost:8080/ap7/org/all"
    return this.http.get(url);  }

  getUser(){
    let url = "http://localhost:8080/ap7/user/all"
    return this.http.get(url);  }
}
