package com.amissible.SampleProduct.Controller;


import com.amissible.SampleProduct.Model.Product;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Product_Controller {


    @GetMapping("/productlist")
    public List<Product> getProduct(){
        List<Product> list = new ArrayList<>();
        list.add(new Product(1,
                "Blue Sanitizer Braclet",
                "blue-sanitizer-braclet",
                "US Health",
                "[2,3]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));

        list.add(new Product(2,
                " Gray Sanitizer Bracelet",
                "gray-sanitizer-braclet",
                "US Health",
                "[3,3]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));

        list.add(new Product(3,
                " Yellow Sanitizer Bracelet",
                "yellow-sanitizer-braclet",
                "US Health",
                "[3,2]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(4,
                " Pink Sanitizer Bracelet",
                "pink-sanitizer-braclet",
                "US Health",
                "[2,1]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(5,
                " Sanitizer Wristband  PVC bracelet ",
                "sanitizer-wristband ",
                "US Health",
                "[4,3]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(6,
                "PVC Gray Sanitizer Wristband  ",
                "sanitizer-wristband ",
                "US Health",
                "[4,4]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(7,
                "PVC Pink Sanitizer Wristband ",
                "sanitizer-wristband ",
                "US Health",
                "[4,1]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(8,
                "Peach Sanitizer Bracelet ",
                "sanitizer-wristband ",
                "US Health",
                "[2,1]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(9,
                " PVC Green Sanitizer Wristband",
                "sanitizer-wristband ",
                "US Health",
                "[1,1]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        list.add(new Product(10,
                "PVC White Sanitizer Wristband ",
                "sanitizer-wristband ",
                "US Health",
                "[1,1]",
                "htps://www.amazon.ca/ajRdtabH",
                "/icon/icon1.jpg",
                "/icon/p1img2.jpg" ,
                "/image/p1img2.jpg",
                "2020-11-03 12:54:00 PM",
                "ACTIVE"));
        return list;

    }
}
