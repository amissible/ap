package com.updatedemo.demo.dao;

import com.updatedemo.demo.model.Product;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ProductDaoImpl implements ProductDao {
    Map<String, Product> productMap = new HashMap<>();

    ProductDaoImpl() {
        Product a = new Product("Something", 40,
                "Something is available to buy for $40 LOL", true);
        productMap.put(a.getName(), a);
        a = new Product("Nothing", 40,
                "Nothing is available to buy for $40 LOL", true);
        productMap.put(a.getName(), a);
        a = new Product("Everything", 40,
                "Everything is available to buy for $40 LOL", true);
        productMap.put(a.getName(), a);
        a = new Product("Anything", 40,
                "Anything is available to buy for $40 LOL", true);
        productMap.put(a.getName(), a);
    }

    @Override
    public boolean saveProduct(Product product) {
        if(productMap.containsKey(product.getName()))
            return false;
        else
            productMap.put(product.getName(), product);
        return true;
    }

    @Override
    public boolean updateProduct(String productName, Product newProduct) {
        if(productMap.containsKey(productName)) {
            Product temp = productMap.get(productName);
            temp.setCurrentPrice(newProduct.getCurrentPrice());
            temp.setAvailable(newProduct.isAvailable());
            temp.setRanking(newProduct.getRanking());
            for(double itr : newProduct.getRatings())
                temp.addRating(itr);
            for(String itr : newProduct.getTags())
                temp.addFilterTags(itr);
            return true;
        } else
            return false;
    }

    @Override
    public boolean deleteProduct(String productName) {
        if(productMap.containsKey(productName)) {
            productMap.remove(productName, productMap.get(productName));
            return true;
        } else
        return false;
    }

    @Override
    public String getProduct(String productName) {
        if(productMap.containsKey(productName))
            return productMap.get(productName).toString();
        else
            return null;
    }

    @Override
    public String viewAll() {
        return productMap.values().toString();
    }
}
