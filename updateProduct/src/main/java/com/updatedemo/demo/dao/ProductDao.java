package com.updatedemo.demo.dao;

import com.updatedemo.demo.model.Product;

public interface ProductDao {
    public boolean saveProduct(Product product);
    public boolean updateProduct(String productName, Product product);
    public boolean deleteProduct(String productName);
    public String getProduct(String productName);
    public String viewAll();
}
