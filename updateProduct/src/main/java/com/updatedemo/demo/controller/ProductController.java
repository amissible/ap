package com.updatedemo.demo.controller;

import com.updatedemo.demo.dao.ProductDao;
import com.updatedemo.demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    @Autowired
    ProductDao productDao;

    @PostMapping("/add")
    public boolean addProduct(@RequestParam Product product) {
        return productDao.saveProduct(product);
    }

    @PostMapping("/remove")
    public boolean remProduct(@RequestParam String name) {
        return productDao.deleteProduct(name);
    }

    @PostMapping("/modify")
    public boolean modifyProduct(@RequestParam String name,
                                 @RequestParam Product product) {
        return productDao.updateProduct(name, product);
    }

    @PostMapping("/view")
    public String viewProduct(@RequestParam String name) {
        return productDao.getProduct(name);
    }

    @PostMapping("/view-all")
    public String viewProduct() {
        return productDao.viewAll();
    }
}