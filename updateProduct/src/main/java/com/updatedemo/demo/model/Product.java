package com.updatedemo.demo.model;

import java.util.List;

public class Product {
    String img_path;
    String name;
    double price;
    double currentPrice;
    String description;
    boolean available;
    List<String> tags;
    List<Double> ratings;
    double totalRatings;
    double avgRating;
    int ranking;
    String category;

    public Product(String name, double price, String description, boolean available) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.available = available;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public List<String> getTags() {
        return tags;
    }

    public void addFilterTags(String tag) {
        this.tags.add(tag);
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Double> getRatings() {
        return ratings;
    }

    public void addRating(double rating) {
        ratings.add(rating);
        calculateAvgRatings();
    }

    //most likely wont be used
    public void setRatings(List<Double> ratings) {
        this.ratings = ratings;
        totalRatings = 0;
        calculateAvgRatings();
    }

    public void calculateAvgRatings() {
        if(totalRatings == 0) {
            for(double itr : ratings) {
                totalRatings += itr;
            }
        }
        else {
            totalRatings += ratings.get(ratings.size()-1);
            avgRating = totalRatings / ratings.size();
        }
    }

    public double getAvgRating() {
        return this.avgRating;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product{" +
                "img_path='" + img_path + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", currentPrice=" + currentPrice +
                ", description='" + description + '\'' +
                ", available=" + available +
                ", tags=" + tags +
                ", ratings=" + ratings +
                ", totalRatings=" + totalRatings +
                ", avgRating=" + avgRating +
                ", ranking=" + ranking +
                ", category='" + category + '\'' +
                '}';
    }
}
