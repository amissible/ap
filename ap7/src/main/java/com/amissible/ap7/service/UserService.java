package com.amissible.ap7.service;

import com.amissible.ap7.exceptions.*;
import com.amissible.ap7.model.User;
import com.amissible.ap7.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<User> getUser(Long id){
        List<User> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM ap7_user WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("org_nickname"), rs.getString("user_id"), rs.getString("user_username"), rs.getString("user_password"), rs.getString("user_status"))
        ).forEach(user -> list.add(user));
        return list;
    }

    public List<User> getAllUser(){
        List<User> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM ap7_user",
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("org_nickname"), rs.getString("user_id"), rs.getString("user_username"), rs.getString("user_password"), rs.getString("user_status"))
        ).forEach(user -> list.add(user));
        return list;
    }

    public Response addUser(String org_nickname,String user_id,String user_username,String user_password,String user_status){
        Long count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_user WHERE org_nickname=? AND user_username=?",new Object[] {org_nickname,user_username},Long.class);
        if(count != 0)
            throw new ConflictException("USER_ALREADY_REGISTERED");
        count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_organization WHERE org_nickname=?",new Object[] {org_nickname},Long.class);
        if(count == 0)
            throw new NotFoundException("ORGANIZATION_NOT_FOUND");
        Integer rows = jdbcTemplate.update("INSERT INTO ap7_user(org_nickname,user_id,user_username,user_password,user_status) VALUES (?,?,?,?,?)",new Object[] {org_nickname,user_id,user_username,user_password,user_status});
        System.out.println(rows);
        if(rows > 0)
            return new Response(true,"USER_ADDED_SUCCESSFULLY");
        else
            throw new InternalServerErrorException("USER_INSERTION_FAILED");
    }

    public Response updateUserOrganization(Long id,String org_nickname){
        Long count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_organization WHERE org_nickname = ?",new Object[] {org_nickname},Long.class);
        if(count == 0)
            throw new NotFoundException("ORGANIZATION_NOT_FOUND");
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE id=?",new Object[] {id},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new NotFoundException("USER_NOT_FOUND");
        count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_user WHERE org_nickname = ? AND user_username = ?",new Object[] {org_nickname,user.get(0).getUser_username()},Long.class);
        if(count != 0)
            throw new ConflictException("ANOTHER_USER_WITH_SIMILAR_CREDENTIALS_ALREADY_REGISTERED");
        int rows = jdbcTemplate.update("UPDATE ap7_user SET org_nickname = ? WHERE id = ?",new Object[] {org_nickname,id});
        if(rows > 0)
            return new Response(true,"USER_ORGANIZATION_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("USER_ORGANIZATION_UPDATE_FAILED");
    }

    public Response updateUserID(Long id,String user_id){
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE id=?",new Object[] {id},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new NotFoundException("USER_NOT_FOUND");
        int rows = jdbcTemplate.update("UPDATE ap7_user SET user_id = ? WHERE id = ?",new Object[] {user_id,id});
        if(rows > 0)
            return new Response(true,"USER_USERID_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("USER_USERID_UPDATE_FAILED");
    }

    public Response updateUserName(Long id,String user_username){
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE id=?",new Object[] {id},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new NotFoundException("USER_NOT_FOUND");
        Long count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_user WHERE org_nickname = ? AND user_username = ?",new Object[] {user.get(0).getOrg_nickname(),user_username},Long.class);
        if(count != 0)
            throw new NotAcceptableException("USERNAME_NOT_AVAILABLE");
        int rows = jdbcTemplate.update("UPDATE ap7_user SET user_username = ? WHERE id = ?",new Object[] {user_username,id});
        if(rows > 0)
            return new Response(true,"USER_USERNAME_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("USER_USERNAME_UPDATE_FAILED");
    }

    public Response updatePassword(Long id,String user_password){
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE id=?",new Object[] {id},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new NotFoundException("USER_NOT_FOUND");
        int rows = jdbcTemplate.update("UPDATE ap7_user SET user_password = ? WHERE id = ?",new Object[] {user_password,id});
        if(rows > 0)
            return new Response(true,"USER_PASSWORD_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("USER_PASSWORD_UPDATE_FAILED");
    }

    public Response updateUserStatus(Long id,String user_status){
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE id=?",new Object[] {id},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new NotFoundException("USER_NOT_FOUND");
        int rows = jdbcTemplate.update("UPDATE ap7_user SET user_status = ? WHERE id = ?",new Object[] {user_status,id});
        if(rows > 0)
            return new Response(true,"USER_STATUS_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("USER_STATUS_UPDATE_FAILED");
    }

    public Response removeUser(Long id){
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE id=?",new Object[] {id},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new NotFoundException("USER_NOT_FOUND");
        int rows = jdbcTemplate.update("DELETE FROM ap7_user WHERE id = ?",new Object[] {id});
        if(rows > 0)
            return new Response(true,"USER_DELETED");
        else
            throw new InternalServerErrorException("USER_DELETION_FAILED");
    }

    public int removeOrgUser(String org_nickname){
        return jdbcTemplate.update("DELETE FROM ap7_user WHERE org_nickname = ?",new Object[] {org_nickname});
    }

    public Response verifyUser(String org_nickname,String user_username,String user_password){
        List<User> user = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_user WHERE org_nickname=? AND user_username=? AND user_password=?",new Object[] {org_nickname,user_username,user_password},(rs,rowNum) -> new User(rs.getLong("id"),rs.getString("org_nickname"),rs.getString("user_id"),rs.getString("user_username"),rs.getString("user_password"),rs.getString("user_status"))).forEach(USER -> user.add(USER));
        if(user.size() == 0)
            throw new UnauthorisedException("USER_NOT_FOUND");
        else if(user.size() > 1)
            throw new ConflictException("DUPLICATE_USER_FOUND");
        else if(user.get(0).getUser_status().equals("ACTIVE"))
            return new Response(true,"USER_AUTHENTIC");
        else
            throw new ForbiddenException("USER_STATUS_NOT_ACTIVE");
    }

    public Long count() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_user",Long.class);
    }

    public Long countUserInOrg(String org_nickname) {
       return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_user WHERE org_nickname = ?",new Object[] {org_nickname},Long.class);
    }
}
