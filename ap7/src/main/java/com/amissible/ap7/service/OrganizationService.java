package com.amissible.ap7.service;

import com.amissible.ap7.exceptions.ConflictException;
import com.amissible.ap7.exceptions.InternalServerErrorException;
import com.amissible.ap7.exceptions.NotFoundException;
import com.amissible.ap7.model.Organization;
import com.amissible.ap7.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrganizationService {

    @Autowired
    private UserService userService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Organization> getOrg(Long id){
        List<Organization> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM ap7_organization WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Organization(rs.getLong("id"), rs.getString("org_name"), rs.getString("org_nickname"))
        ).forEach(org -> list.add(org));
        return list;
    }

    public List<Organization> getAllOrg(){
        List<Organization> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM ap7_organization",
                (rs, rowNum) -> new Organization(rs.getLong("id"), rs.getString("org_name"), rs.getString("org_nickname"))
        ).forEach(org -> list.add(org));
        return list;
    }

    public Response addOrganization(String org_name, String org_nickname){
        Long count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_organization WHERE org_nickname = ?",new Object[] {org_nickname},Long.class);
        if(count != 0)
            throw new ConflictException("ORGANIZATION_NICKNAME_NOT_AVAILABLE");
        int rows = jdbcTemplate.update("INSERT INTO ap7_organization(org_name,org_nickname) VALUES (?,?)",new Object[] {org_name,org_nickname});
        if(rows > 0)
            return new Response(true,"ORGANIZATION_ADDED_SUCCESSFULLY");
        else
            throw new InternalServerErrorException("ORGANIZATION_INSERTION_FAILED");
    }

    public Response updateOrgName(Long id,String org_name){
        List<Organization> org = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_organization WHERE id=?",new Object[] {id},(rs,rowNum) -> new Organization(rs.getLong("id"),rs.getString("org_name"),rs.getString("org_nickname"))).forEach(ORG -> org.add(ORG));
        if(org.size() == 0)
            throw new NotFoundException("ORGANIZATION_NOT_FOUND");
        int rows = jdbcTemplate.update("UPDATE ap7_organization SET org_name = ? WHERE id = ?",new Object[] {org_name,id});
        if(rows > 0)
            return new Response(true,"ORGANIZATION_NAME_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("ORGANIZATION_NAME_UPDATE_FAILED");
    }

    public Response updateOrgNickname(Long id,String org_nickname){
        List<Organization> org = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_organization WHERE id=?",new Object[] {id},(rs,rowNum) -> new Organization(rs.getLong("id"),rs.getString("org_name"),rs.getString("org_nickname"))).forEach(ORG -> org.add(ORG));
        if(org.size() == 0)
            throw new NotFoundException("ORGANIZATION_NOT_FOUND");
        Long count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_organization WHERE org_nickname = ?",new Object[] {org_nickname},Long.class);
        if(count > 0)
            throw new ConflictException("ORGANIZATION_NICKNAME_NOT_AVAILABLE");
        int rows = jdbcTemplate.update("UPDATE ap7_organization SET org_nickname = ? WHERE id = ?",new Object[] {org_nickname,id});
        if(rows > 0)
            return new Response(true,"ORGANIZATION_NICKNAME_UPDATE_SUCCESS");
        else
            throw new InternalServerErrorException("ORGANIZATION_NICKNAME_UPDATE_FAILED");
    }

    public Response removeOrg(Long id){
        List<Organization> org = new ArrayList<>();
        jdbcTemplate.query("SELECT * FROM ap7_organization WHERE id=?",new Object[] {id},(rs,rowNum) -> new Organization(rs.getLong("id"),rs.getString("org_name"),rs.getString("org_nickname"))).forEach(ORG -> org.add(ORG));
        if(org.size() == 0)
            throw new NotFoundException("ORGANIZATION_NOT_FOUND");
        userService.removeOrgUser(org.get(0).getOrg_nickname());
        int rows = jdbcTemplate.update("DELETE FROM ap7_organization WHERE id = ?",new Object[] {id});
        if(rows > 0)
            return new Response(true,"ORGANIZATION_DELETED");
        else
            throw new InternalServerErrorException("ORGANIZATION_DELETION_FAILED");
    }

    public Long count() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ap7_organization",Long.class);
    }
}