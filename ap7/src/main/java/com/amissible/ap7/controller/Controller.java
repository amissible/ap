package com.amissible.ap7.controller;

import com.google.gson.Gson;
import com.amissible.ap7.service.UserService;
import com.amissible.ap7.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ap7") //App level Mapping to ensure there is no path conflict is multiple applications run concurrently
public class Controller {

    @Autowired
    private UserService userService; //Handles all actions related to user table

    @Autowired
    private OrganizationService organizationService; //Handles all actions related to organization table

    // Default path for the module
    @RequestMapping("/")
    String get(){
        String ap7 = "<h1>AP7 - Login Authentication</h1>Contact Sales Team for full details";
        return ap7;
    }

    // Gives detail of a particular Organization
    @RequestMapping(value = "/org")
    String getOrganization(@RequestParam("id") Long id){
        Gson gson = new Gson();
        return gson.toJson(organizationService.getOrg(id));
    }

    // Gives detail of all Organizations
    @RequestMapping(value = "/org/all")
    String getAllOrganization(){
        Gson gson = new Gson();
        return gson.toJson(organizationService.getAllOrg());
    }

    // Adds a new Organization
    @RequestMapping(value = "/org/add")
    String addOrganization(@RequestParam("name") String org_name, @RequestParam("nickname") String org_nickname){
        Gson gson = new Gson();
        return gson.toJson(organizationService.addOrganization(org_name,org_nickname));
    }

    // Updates name of Organization
    @RequestMapping(value = "/org/update/name")
    String updateOrgName(@RequestParam("id") Long id,@RequestParam("name") String org_name){
        Gson gson = new Gson();
        return gson.toJson(organizationService.updateOrgName(id,org_name));
    }

    // Updates nickname of Organization
    @RequestMapping(value = "/org/update/nickname")
    String updateOrgNickname(@RequestParam("id") Long id,@RequestParam("nickname") String org_nickname){
        Gson gson = new Gson();
        return gson.toJson(organizationService.updateOrgNickname(id,org_nickname));
    }

    // Removes an Organization
    @RequestMapping(value = "/org/remove")
    String removeOrganization(@RequestParam("id") Long id){
        Gson gson = new Gson();
        return gson.toJson(organizationService.removeOrg(id));
    }

    // Gives detail of a particular User
    @RequestMapping(value = "/user")
    String getUser(@RequestParam("id") Long id){
        Gson gson = new Gson();
        return gson.toJson(userService.getUser(id));
    }

    // Gives detail of all Users
    @RequestMapping(value = "/user/all")
    String getAllUser(){
        Gson gson = new Gson();
        return gson.toJson(userService.getAllUser());
    }

    // Adds a new User
    @RequestMapping(value = "/user/add")
    String addUser(@RequestParam("org") String org_nickname,@RequestParam("userID") String user_id,@RequestParam("username") String user_username,@RequestParam("password") String user_password,@RequestParam("status") String user_status){
        Gson gson = new Gson();
        return gson.toJson(userService.addUser(org_nickname,user_id,user_username,user_password,user_status));
    }

    // Updates organization of User
    @RequestMapping(value = "/user/update/org")
    String updateUserOrganization(@RequestParam("id") Long id,@RequestParam("org") String org_nickname){
        Gson gson = new Gson();
        return gson.toJson(userService.updateUserOrganization(id,org_nickname));
    }

    // Updates user-id of User
    @RequestMapping(value = "/user/update/userid")
    String updateUserID(@RequestParam("id") Long id,@RequestParam("userID") String user_id){
        Gson gson = new Gson();
        return gson.toJson(userService.updateUserID(id,user_id));
    }

    // Updates username of User
    @RequestMapping(value = "/user/update/username")
    String updateUserName(@RequestParam("id") Long id,@RequestParam("username") String user_username){
        Gson gson = new Gson();
        return gson.toJson(userService.updateUserName(id,user_username));
    }

    // Updates password of User
    @RequestMapping(value = "/user/update/password")
    String updateUserPassword(@RequestParam("id") Long id,@RequestParam("password") String user_password){
        Gson gson = new Gson();
        return gson.toJson(userService.updatePassword(id,user_password));
    }

    // Updates status of User
    @RequestMapping(value = "/user/update/status")
    String updateUserStatus(@RequestParam("id") Long id,@RequestParam("status") String user_status){
        Gson gson = new Gson();
        return gson.toJson(userService.updateUserStatus(id,user_status));
    }

    // Removes User
    @RequestMapping(value = "/user/remove")
    String removeUser(@RequestParam("id") Long id){
        Gson gson = new Gson();
        return gson.toJson(userService.removeUser(id));
    }

    // Validates user credentials for Authentication Purpose
    @RequestMapping(value = "/user/validate")
    String verifyUser(@RequestParam("org") String org_nickname,@RequestParam("username") String user_username,@RequestParam("password") String user_password){
        Gson gson = new Gson();
        return gson.toJson(userService.verifyUser(org_nickname,user_username,user_password));
    }

    // Gives count of Organizations in database
    @RequestMapping(value = "/stats/count/org")
    Long getOrganizationCount(){
        return organizationService.count();
    }

    // Gives count of Users in database
    @RequestMapping(value = "/stats/count/user")
    Long getUserCount(){
        return userService.count();
    }

    // Gives count of Users in a particular Organization
    @RequestMapping(value = "/stats/count/orguser")
    Long getUserInOrgCount(@RequestParam("org") String org_nickname){
        return userService.countUserInOrg(org_nickname);
    }
}
