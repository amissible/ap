package com.amissible.ap7.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String org_nickname;
    private String user_id;
    private String user_username;
    private String user_password;
    private String user_status;

    public User() {}

    public User(Long id, String org_nickname, String user_id, String user_username, String user_password, String user_status) {
        this.id = id;
        this.org_nickname = org_nickname;
        this.user_id = user_id;
        this.user_username = user_username;
        this.user_password = user_password;
        this.user_status = user_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrg_nickname() {
        return org_nickname;
    }

    public void setOrg_nickname(String org_nickname) {
        this.org_nickname = org_nickname;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_username() {
        return user_username;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder("USER{");
        sb.append("id=").append(id);
        sb.append(", org_nickname='").append(org_nickname).append('\'');
        sb.append(", user_id='").append(user_id).append('\'');
        sb.append(", user_username='").append(user_username).append('\'');
        sb.append(", user_password='").append(user_password).append('\'');
        sb.append(", user_status='").append(user_status).append('\'');
        sb.append('}');
        return sb.toString();
    }
}