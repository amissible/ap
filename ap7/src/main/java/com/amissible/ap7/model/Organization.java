package com.amissible.ap7.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String org_name;
    private String org_nickname;

    public Organization() {}

    public Organization(Long id, String org_name, String org_nickname) {
        this.id = id;
        this.org_name = org_name;
        this.org_nickname = org_nickname;
    }

    public Organization(String org_name, String org_nickname) {
        this.org_name = org_name;
        this.org_nickname = org_nickname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_nickname() {
        return org_nickname;
    }

    public void setOrg_nickname(String org_nickname) {
        this.org_nickname = org_nickname;
    }

    public String toString() {
        final StringBuilder sb = new StringBuilder("ORG{");
        sb.append("id=").append(id);
        sb.append(", org_name='").append(org_name).append('\'');
        sb.append(", org_nickname='").append(org_nickname).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
