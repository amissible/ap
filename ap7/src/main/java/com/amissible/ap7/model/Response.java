package com.amissible.ap7.model;

public class Response {
    Boolean actionStatus;
    Integer code;
    String message;

    public Response(Boolean actionStatus, String message) {
        this.actionStatus = actionStatus;
        this.code = 200;
        this.message = message;
    }
}