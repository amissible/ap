package com.amissible.auth.service;

import com.amissible.auth.model.LoginBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.http.HttpRequest;

@RestController("/login")
public class LoginController {

    @RequestMapping(method = RequestMethod.POST,path = "validate")
    public LoginBean performLogin(@RequestBody LoginBean loginInfo){
        return loginInfo;
    }
}
