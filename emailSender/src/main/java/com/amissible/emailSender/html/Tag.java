package com.amissible.emailSender.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Set;

public class Tag {

    private String tag;
    private Map<String,String> attributes;
    private String innerHTML;
    private List<Tag> children;

    public Tag() {
        tag = "";
        attributes = new HashMap<>();
        innerHTML = "";
        children = new ArrayList<>();
    }

    public Tag(String tag) {
        this.tag = tag;
        attributes = new HashMap<>();
        innerHTML = "";
        children = new ArrayList<>();
    }

    public Tag(String tag, Map<String, String> attributes) {
        this.tag = tag;
        this.attributes = attributes;
        innerHTML = "";
        children = new ArrayList<>();
    }

    public Tag(String tag, String innerHTML) {
        this.tag = tag;
        attributes = new HashMap<>();
        this.innerHTML = innerHTML;
        children = new ArrayList<>();
    }

    public Tag(String tag, Map<String, String> attributes, String innerHTML) {
        this.tag = tag;
        this.attributes = attributes;
        this.innerHTML = innerHTML;
        children = new ArrayList<>();
    }

    public Tag(String tag, Map<String, String> attributes, List<Tag> children) {
        this.tag = tag;
        this.attributes = attributes;
        innerHTML = "";
        this.children = children;
    }

    public Tag(String tag, Map<String, String> attributes, String innerHTML, List<Tag> children) {
        this.tag = tag;
        this.attributes = attributes;
        this.innerHTML = innerHTML;
        this.children = children;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getAttribute(String attribute){
        return this.attributes.get(attribute);
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(String attribute,String value) {
        this.attributes.put(attribute,value);
    }

    public String getInnerHTML() {
        return innerHTML;
    }

    public void setInnerHTML(String innerHTML) {
        this.innerHTML = innerHTML;
    }

    public List<Tag> getChildren() {
        return children;
    }

    public Tag getChild(Integer index) {
        return children.get(index);
    }

    public void setChildren(List<Tag> children) {
        this.children = children;
    }

    public void addChild(Tag child) {
        children.add(child);
    }

    public String getContent(){
        String html = "<" + tag;
        Set<String> keySet = attributes.keySet();
        for(String key : keySet)
            html += " " + key + "='" + attributes.get(key) + "'";
        html += ">" + innerHTML;
        for(Tag child : children)
            html += child.getContent();
        html += "</" + tag + ">";
        return html;
    }
}
