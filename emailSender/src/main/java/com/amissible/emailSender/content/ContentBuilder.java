package com.amissible.emailSender.content;

import java.util.Map;
import java.util.Set;

public class ContentBuilder {

    private String content;

    public ContentBuilder() {
        content = "";
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public void add(String content){
        this.content += content;
    }

    public void addHTMLElement(String tag, String innerHTML, Map<String,String> attributes){
        String html = "<" + tag;
        Set<String> keySet = attributes.keySet();
        for(String key : keySet)
            html += " " + key + "='" + attributes.get(key) + "'";
        html += ">" + innerHTML + "</" + tag + ">";
        content += html;
    }

    public void addParagraph(String text,String style){
        content += "<p style='" + style + "'>" + text + "</p>";
    }

    public void addParagraph(String text){
        content += "<p>" + text + "</p>";
    }

    public void addTable(String table[][]){
        String html = "<table>";
        for(int i=0;i< table.length;i++){
            html += "<tr>";
            for(int j=0;j<table[0].length;j++)
                html += "<td>" + table[i][j] + "</td>";
            html += "</tr>";
        }
        html += "</table>";
        content += html;
    }

    public void addImage(String src){
        content += "<img src='" + src + "' />";
    }

    public void addLink(String message,String href){
        content += "<a href='" + href + "' target='_blank'>" + message + "</a>";
    }
}
