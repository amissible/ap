package com.amissible.emailSender.controller;

import com.amissible.emailSender.content.ContentBuilder;
import com.amissible.emailSender.html.Tag;
import com.amissible.emailSender.service.EmailService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class EmailController {

    @RequestMapping(value = "/sendNumericOTP")
    public String sendNumericOTP(@RequestParam String email){
        EmailService emailService = new EmailService();
        return emailService.sendNumericOTP(email);
    }

    @RequestMapping(value = "/sendAlphaNumericOTP")
    public String sendAlphaNumericOTP(@RequestParam String email){
        EmailService emailService = new EmailService();
        return emailService.sendAlphaNumericOTP(email);
    }

    @RequestMapping(value = "/sendNotification")
    public void sendAlphaNumericOTP(@RequestParam String email,@RequestParam String subject,@RequestParam String content){
        EmailService emailService = new EmailService();
        emailService.sendSimpleEmail(email,subject,content);
    }

    @RequestMapping(value = "/sendEmail")
    public void sendEmail(@RequestParam String to,
                          @RequestParam String cc,
                          @RequestParam String bcc,
                          @RequestParam String subject,
                          @RequestParam String content) {
        EmailService emailService = new EmailService();
        emailService.sendEmail(to,cc,bcc,subject,content,null);
    }

    @RequestMapping(value = "/sendEmailWithAttachment")
    public void sendEmail(@RequestParam String to,
                          @RequestParam String cc,
                          @RequestParam String bcc,
                          @RequestParam String subject,
                          @RequestParam String content,
                          @RequestParam String attachments) {
        EmailService emailService = new EmailService();
        emailService.sendEmail(to,cc,bcc,subject,content,attachments.split(","));
    }

    @RequestMapping(value = "/sendEmailTest")
    public void sendEmailTest() {
        EmailService emailService = new EmailService();
        emailService.sendEmail(
                "amit.shukla@amissible.com,alok.shukla@amissible.com,vijendra.singh@amissible.com,anwar.mehmood@amissible.com",
                "omendra.sgakkar@amissible.com,sparsh.mishra@amissible.com,mohammad.umair@amissible.com",
                "urvashi.dubey@amissible.com,sushmita.singh@amissible.com",
                "Greetings from Amissible",
                "Hello,<br><br>Email service is functioning.<br><br>Regards,<br><hr><b>Omendra Singh Gakkhar</b><br><small>Amissible Software LLP<br>Cell:+91-800-436-9336 | <a href='http://www.amissible.com/' target='_blank'>www.amissible.com</a></small><hr>",
                new String[]{"/micky-mouse.jpg","/tom-and-jerry.jpg","/oggy-and-cockroaches.jpg"});
    }

    @RequestMapping(value = "/tagTest")
    public void tagTest() {
        ContentBuilder contentBuilder = new ContentBuilder();

        contentBuilder.addParagraph("Hello, this is test","color:red;");

        Map<String,String> attr = new HashMap<>();
        attr.put("border","1");
        attr.put("style","background:yellow;color:red;");

        Map<String,String> cellAttr = new HashMap<>();
        cellAttr.put("style","padding:10px;");

        Tag table = new Tag("table",attr);
        Tag tr = new Tag("tr");
        tr.addChild(new Tag("td",cellAttr,"First Cell"));
        tr.addChild(new Tag("td",cellAttr,"Second Cell"));
        tr.addChild(new Tag("td",cellAttr,"Third Cell"));
        table.addChild(tr);
        table.addChild(tr);
        table.addChild(tr);

        contentBuilder.add(table.getContent());

        System.out.println(contentBuilder.getContent());

        EmailService emailService = new EmailService();
        emailService.sendEmail(
                "omendra.sgakkar@amissible.com",
                null,
                null,
                "Greetings from Amissible",
                contentBuilder.getContent(),
                new String[]{"/micky-mouse.jpg","/tom-and-jerry.jpg","/oggy-and-cockroaches.jpg"});
    }

}
