package com.amissible.mnp.controllers;

import com.amissible.mnp.daos.EmailService;
import com.amissible.mnp.models.Appointment;
import com.amissible.mnp.repositories.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/appointments")
public class AppointmentController {

    @Autowired
    private EmailService emailService;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @PostMapping("/create_appointment")
    ResponseEntity<Appointment> create(@RequestParam String email, @RequestParam String dueOn, @RequestParam Double amountPaid) throws IOException, MessagingException {
        Appointment appointment = new Appointment();
        appointment.setEmail(email);
        appointment.setDueOn(dueOn);
        if(amountPaid > 0.0) {
            appointment.setAmountPaid(amountPaid);
            appointment.setPaymentStatus("PAID");
            //send email notifying that amount is paid.
            String subject = "Scheduled appointment alert!";
            String message = "Hello, \n Welcome to MnP, your billed amount was " + amountPaid +
                    " (" + appointment.getPaymentStatus() + ") and your appointment is due on " + dueOn;
            emailService.sendMessage(email, subject, message);
        } else {
            appointment.setAmountPaid(0.0);
            appointment.setPaymentStatus("PENDING");
            //send email notifying that your payment is pending

            String subject = "Scheduled appointment alert!";
            String message = "Hello, \n Welcome to MnP, your billed amount was " + amountPaid +
                    " (" + appointment.getPaymentStatus() + "). Complete the payment to schedule the appointment.";
            emailService.sendMessage(email, subject, message);
        }
        appointmentRepository.save(appointment);
        return new ResponseEntity(appointment, HttpStatus.CREATED);
    }

    @GetMapping("/view_appointments")
    List<Appointment> findAll() {
        return appointmentRepository.findAll();
    }
}
