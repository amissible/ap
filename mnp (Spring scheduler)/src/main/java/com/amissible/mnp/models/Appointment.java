package com.amissible.mnp.models;

import javax.persistence.*;

@Entity
@Table(name = "APPOINTMENT")
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "due_on")
    private String dueOn;

    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "amount")
    private Double amountPaid;

    public Appointment() {
    }

    public Appointment(Long id, String email, String dueOn, String paymentStatus, Double amountPaid) {
        this.id = id;
        this.email = email;
        this.dueOn = dueOn;
        this.paymentStatus = paymentStatus;
        this.amountPaid = amountPaid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDueOn() {
        return dueOn;
    }

    public void setDueOn(String dueOn) {
        this.dueOn = dueOn;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Double amountPaid) {
        this.amountPaid = amountPaid;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", dueOn='" + dueOn + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", amountPaid=" + amountPaid +
                '}';
    }
}
