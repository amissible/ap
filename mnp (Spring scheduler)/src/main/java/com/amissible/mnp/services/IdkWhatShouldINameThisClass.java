package com.amissible.mnp.services;

import com.amissible.mnp.daos.EmailService;
import com.amissible.mnp.models.Appointment;
import com.amissible.mnp.repositories.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class IdkWhatShouldINameThisClass {
    //reference (cron expressions: second, minute, hour, day of month, month, day(s) of week)

    @Autowired
    EmailService emailService;

    @Autowired
    AppointmentRepository appointmentRepository;

    @Scheduled(cron = "0 5 19 * * ?")
    void routine() throws ParseException, IOException, MessagingException {
        List<Appointment> appointments = appointmentRepository.findAll();
        String subject = "Scheduled appointment alert!";
        for(Appointment itr : appointments) {
            Date date1= new SimpleDateFormat("dd/MM/yyyy").parse(itr.getDueOn());
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date2 = new Date();
            String message = "Hello, \n Welcome to MnP, your appointment is due in " + getDifferenceDays(date1, date2) + " days.";
            emailService.sendMessage(itr.getEmail(), subject, message);
        }
    }

    public long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
