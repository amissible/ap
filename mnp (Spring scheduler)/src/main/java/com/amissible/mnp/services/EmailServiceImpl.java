package com.amissible.mnp.services;

import com.amissible.mnp.daos.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void sendMessage(String to, String subject, String text) throws IOException, MessagingException {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setFrom("dontbothertoreply@me.com");
        msg.setTo(to);
        msg.setSubject(subject);
        msg.setText(text);
        mailSender.send(msg);
    }
}
