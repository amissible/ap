package com.amissible.appointment.service;

import com.amissible.appointment.dao.*;
import com.amissible.appointment.exception.AppException;
import com.amissible.appointment.model.*;
import com.amissible.appointment.payload.AppointmentSlot;
import com.amissible.appointment.util.Job;
import com.amissible.appointment.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AppointmentService {

    @Autowired
    EmailService emailService;

    @Autowired
    AppointmentDao appointmentDao;

    @Autowired
    HostDao hostDao;

    @Autowired
    GuestDao guestDao;

    @Autowired
    TypeDao typeDao;

    @Autowired
    TypeConfigDao typeConfigDao;

    @Autowired
    PaymentDao paymentDao;

    @Autowired
    HolidayDao holidayDao;

    @Autowired
    Job job;

    public void start(){
        job.start();
    }

    public void destroy(){
        appointmentDao.dropTable();
        hostDao.dropTable();
        guestDao.dropTable();
        typeDao.dropTable();
        typeConfigDao.dropTable();
        paymentDao.dropTable();
    }

    public void registerHost(Host host){
        hostDao.save(host);
    }

    public void updateHost(Host host){
        hostDao.update(host);
    }

    public List<Host> getActiveHosts() {
        return hostDao.getHostsByStatus("ACTIVE");
    }

    public void registerGuest(Guest guest){
        guestDao.save(guest);
    }

    public void updateGuest(Guest guest){
        guestDao.update(guest);
    }

    public void addType(Type type){
        typeDao.save(type);
    }

    public void updateType(Type type){
        typeDao.update(type);
    }

    public void addTypeConfig(TypeConfig typeConfig[]){
        for(int i=0;i<typeConfig.length;i++)
            typeConfigDao.save(typeConfig[i]);
    }

    public void updateTypeConfig(TypeConfig typeConfig){
        typeConfigDao.update(typeConfig);
    }

    public void addHoliday(Holiday holiday){
        holidayDao.save(holiday);
    }

    public void updateHoliday(Holiday holiday){
        holidayDao.update(holiday);
    }

    public Map<String, List<AppointmentSlot>> getAppointmentsInfo(Long hostId){
        Map<String, List<AppointmentSlot>> map = new HashMap<>();
        Host host = hostDao.getHostById(hostId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Calendar calendar = Calendar.getInstance();
        for(Integer i=0;i<=host.getLeadDays();i++){
            String date = simpleDateFormat.format(calendar.getTime());
            List<Appointment> appointments = appointmentDao.getAppointmentsByDate(date);
            List<AppointmentSlot> appointmentSlots = new ArrayList<>();
            for(Appointment appointment : appointments){
                AppointmentSlot slot = new AppointmentSlot();
                slot.setId(appointment.getId());
                slot.setFees(appointment.getFees());
                slot.setName(appointment.getName());
                slot.setDescription(appointment.getDescription());
                slot.setLabel(appointment.getLabel());
                slot.setStartTime(appointment.getStartTime());
                slot.setEndTime(appointment.getEndTime());
                slot.setStatus(appointment.getStatus());
                appointmentSlots.add(slot);
            }
            map.put(date,appointmentSlots);
            calendar.add(Calendar.DAY_OF_YEAR,1);
        }
        return map;
    }

    public RedirectView bookAppointment(Long appointmentId,Long guestId,String guestMessage){
        Appointment appointment = appointmentDao.getAppointmentById(appointmentId);
        if(appointment == null)
            throw new AppException("APPOINTMENT_NOT_FOUND",HttpStatus.NOT_FOUND);
        Guest guest = guestDao.getGuestById(guestId);
        if(guest == null)
            throw new AppException("GUEST_NOT_FOUND",HttpStatus.NOT_FOUND);
        if(!appointment.getStatus().equals("OPEN"))
            throw new AppException("APPOINTMENT_NOT_OPEN", HttpStatus.BAD_REQUEST);
        appointment.setGuestId(guestId);
        appointment.setGuestMessage(guestMessage);
        appointment.setBookTimeStamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        appointment.setStatus("PROCESSING");
        appointmentDao.update(appointment);
        return new RedirectView("http://localhost:8083/index?alias=APPOINTMENT&amount="+appointment.getFees()+"&currency=INR&token="+guest.getId()+"-"+appointment.getId());
    }

    public RedirectView confirmAppointment(Long guestId,Long appointmentId,String state,Float txnAmount,String txnId){
        Payment payment = new Payment();
        payment.setTxnId(txnId);
        payment.setTxnAmount(txnAmount);
        if(state.equalsIgnoreCase("completed"))
            payment.setTxnStatus("SUCCESS");
        else
            payment.setTxnStatus("UNCLEAR");
        payment.setTxnTimeStamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        payment.setGuestId(guestId);
        payment.setAppointmentId(appointmentId);
        paymentDao.save(payment);
        payment = paymentDao.getPaymentByTransactionId(txnId);
        if(payment == null)
            throw new AppException("PAYMENT_NOT_CAPTURED",HttpStatus.INTERNAL_SERVER_ERROR);
        if(!payment.getTxnStatus().equals("SUCCESS"))
            throw new AppException("PAYMENT_NOT_CONFIRMED",HttpStatus.NOT_ACCEPTABLE);
        Appointment appointment = appointmentDao.getAppointmentById(appointmentId);
        if(appointment == null)
            throw new AppException("APPOINTMENT_NOT_FOUND",HttpStatus.INTERNAL_SERVER_ERROR);
        if(appointment.getGuestId() != guestId)
            throw new AppException("GUEST_MISMATCH",HttpStatus.CONFLICT);
        if(!appointment.getStatus().equals("PROCESSING"))
            throw new AppException("APPOINTMENT_STATUS_TAMPERED",HttpStatus.EXPECTATION_FAILED);
        appointment.setStatus("BOOKED");
        appointment.setPayId(payment.getId());
        appointmentDao.update(appointment);

        Host host = hostDao.getHostById(appointment.getHostId());
        Guest guest = guestDao.getGuestById(appointment.getGuestId());

        Utility.generateICS(appointmentId,"APPOINTMENT : "+appointment.getName(),appointment.getLabel(),"Gorakhpur",
                host.getName(),host.getEmail(),guest.getName(),guest.getEmail(),"Asia/Kolkata",
                appointment.getDate(),appointment.getStartTime(),appointment.getEndTime(),
                "/app/export/appointment/appointment"+appointmentId+".ics");

        Mail mail = new Mail();
        mail.setTo(guest.getEmail());
        mail.setSubject("APPOINTMENT CONFIRMED");
        mail.setTemplate("appointment-confirm.ftl");
        mail.setAttachments(new String[]{"/app/export/appointment/appointment"+appointmentId+".ics"});
        Map<String,String> map = new HashMap<>();
        map.put("name",guest.getName());
        map.put("date",appointment.getDate());
        map.put("start",appointment.getStartTime());
        map.put("end",appointment.getEndTime());
        map.put("bookTimeStamp",appointment.getBookTimeStamp());
        map.put("guestMessage",appointment.getGuestMessage());
        map.put("hostName",host.getName());
        map.put("designation",host.getDesignation());
        map.put("email",host.getEmail());
        map.put("about",host.getAbout());
        map.put("txnId", payment.getTxnId());
        map.put("txnAmount", payment.getTxnAmount().toString());
        map.put("txnStatus", payment.getTxnStatus());
        map.put("txnTimeStamp", payment.getTxnTimeStamp());
        mail.setModel(map);
        emailService.sendSimpleMessage(mail);

        return new RedirectView("https://www.amissible.com?guestId="+guestId+"&appointmentId="+appointmentId);
    }

    public RedirectView clearAppointment(Long guestId,Long appointmentId){
        Appointment appointment = appointmentDao.getAppointmentById(appointmentId);
        if(appointment == null)
            throw new AppException("APPOINTMENT_NOT_FOUND",HttpStatus.INTERNAL_SERVER_ERROR);
        if(appointment.getGuestId() != guestId)
            throw new AppException("GUEST_MISMATCH",HttpStatus.CONFLICT);
        appointment.setGuestId(0L);
        appointment.setGuestMessage("");
        appointment.setBookTimeStamp("");
        appointment.setStatus("OPEN");
        appointmentDao.update(appointment);
        return new RedirectView("https://www.amissible.com?guestId="+guestId);
    }

}
