package com.amissible.appointment.service;

import com.amissible.appointment.dao.*;
import com.amissible.appointment.model.*;
import com.amissible.appointment.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class JobService {

    @Autowired
    EmailService emailService;

    @Autowired
    AppointmentDao appointmentDao;

    @Autowired
    HostDao hostDao;

    @Autowired
    GuestDao guestDao;

    @Autowired
    TypeDao typeDao;

    @Autowired
    TypeConfigDao typeConfigDao;

    @Autowired
    PaymentDao paymentDao;

    @Autowired
    HolidayDao holidayDao;

    public void createTables(){
        appointmentDao.createTable();
        hostDao.createTable();
        guestDao.createTable();
        typeDao.createTable();
        typeConfigDao.createTable();
        paymentDao.createTable();
        holidayDao.createTable();
    }

    public void generateAppointmentSlots(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        List<Host> hosts = hostDao.getHostsByStatus("ACTIVE");
        List<Type> types = typeDao.getAllTypes();
        List<Holiday> holidays = holidayDao.getAllUpcomingHolidays(simpleDateFormat.format(new Date()));
        Map<Long,Type> typeMap =  new HashMap<>();
        for(Type t : types)
            typeMap.put(t.getId(),t);
        for(Host host : hosts){
            Type type = typeMap.get(host.getTypeId());
            if(type == null || !type.getStatus().equals("ACTIVE"))
                continue;
            List<TypeConfig> typeConfigs = typeConfigDao.getTypeConfigsByTypeId(host.getTypeId());
            Calendar calendar = Calendar.getInstance();
            List<Appointment> appointmentList = appointmentDao.getNextAppointmentsByHostId(host.getId(),simpleDateFormat.format(calendar.getTime()));
            System.out.println("Size = "+appointmentList.size());
            for(Integer i=0;i<=host.getLeadDays();i++){
                String date = simpleDateFormat.format(calendar.getTime());
                if(Utility.isHoliday(date,holidays))
                    continue;
                String day = Utility.getDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));
                for(TypeConfig typeConfig : typeConfigs){
                    if(!typeConfig.getDay().equals(day))
                        continue;
                    Boolean exists = false;
                    for(Appointment appointment : appointmentList){
                        if(appointment.getDate().equals(date) && appointment.getStartTime().equals(typeConfig.getStartTime()))
                            exists = true;
                        if(exists)
                            break;
                    }
                    if(exists)
                        continue;
                    Appointment appointment = new Appointment();
                    appointment.setHostId(host.getId());
                    appointment.setFees(typeConfig.getFees());
                    if(appointment.getFees() == 0.0F)
                        appointment.setFees(host.getFees());
                    appointment.setDate(date);
                    appointment.setName(type.getName());
                    appointment.setDescription(type.getDescription());
                    appointment.setLabel(typeConfig.getLabel());
                    appointment.setStartTime(typeConfig.getStartTime());
                    appointment.setEndTime(typeConfig.getEndTime());
                    appointment.setStatus("OPEN");
                    System.out.println(appointment.toString());
                    appointmentDao.save(appointment);
                }
                calendar.add(Calendar.DAY_OF_YEAR,1);
            }
        }
    }


}
