package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.HolidayDao;
import com.amissible.appointment.model.Appointment;
import com.amissible.appointment.model.Holiday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HolidayDaoImpl implements HolidayDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_HOLIDAY";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`date` varchar(11) NOT NULL,`name` varchar(100) NOT NULL)";
    private final String getAllHolidays = "SELECT * FROM "+tableName;
    private final String getAllUpcomingHolidays = "SELECT * FROM "+tableName+" WHERE `date`>=?";
    private final String getHolidayById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String update = "UPDATE "+tableName+" SET date=?,name=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(date,name) VALUES(?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Holiday> getAllHolidays(){
        return jdbcTemplate.query(getAllHolidays,new Object[]{},(rs,rowNum) -> new Holiday(rs.getLong("id"),rs.getString("date"),rs.getString("name")));
    }

    public List<Holiday> getAllUpcomingHolidays(String date){
        return jdbcTemplate.query(getAllUpcomingHolidays,new Object[]{date},(rs,rowNum) -> new Holiday(rs.getLong("id"),rs.getString("date"),rs.getString("name")));
    }

    public Holiday getHolidayById(Long id){
        List<Holiday> holidays = jdbcTemplate.query(getHolidayById,new Object[]{id},(rs, rowNum) -> new Holiday(rs.getLong("id"),rs.getString("date"),rs.getString("name")));
        if(holidays.size() == 0)
            return null;
        return holidays.get(0);
    }

    public void update(Holiday holiday){
        jdbcTemplate.update(update,holiday.getDate(),holiday.getName(),holiday.getId());
    }

    public void save(Holiday holiday){
        jdbcTemplate.update(save,holiday.getDate(),holiday.getName());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
