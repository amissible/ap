package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.GuestDao;
import com.amissible.appointment.model.Guest;
import com.amissible.appointment.model.TypeConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuestDaoImpl implements GuestDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_GUEST";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`uid` varchar(25) NOT NULL,`name` varchar(100) NOT NULL,`about` varchar(255) NOT NULL,`email` varchar(50) NOT NULL,`contact` varchar(20) NOT NULL)";
    private final String getAllGuests = "SELECT * FROM "+tableName;
    private final String getGuestById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getGuestByUid = "SELECT * FROM "+tableName+" WHERE uid=?";
    private final String getGuestsByName = "SELECT * FROM "+tableName+" WHERE name LIKE ?";
    private final String update = "UPDATE "+tableName+" SET uid=?,name=?,about=?,email=?,contact=? WHERE id=?";
    private final String updateByUid = "UPDATE "+tableName+" SET name=?,about=?,email=?,contact=? WHERE uid=?";
    private final String save = "INSERT INTO "+tableName+"(uid,name,about,email,contact) VALUES(?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByUid = "DELETE FROM "+tableName+" WHERE uid=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Guest> getAllGuests(){
        return jdbcTemplate.query(getAllGuests,new Object[]{},(rs,rowNum) -> new Guest(rs.getLong("id"),rs.getString("uid"),rs.getString("name"),rs.getString("about"),rs.getString("email"),rs.getString("contact")));
    }

    public Guest getGuestById(Long id){
        List<Guest> guests = jdbcTemplate.query(getGuestById,new Object[]{id},(rs,rowNum) -> new Guest(rs.getLong("id"),rs.getString("uid"),rs.getString("name"),rs.getString("about"),rs.getString("email"),rs.getString("contact")));
        if(guests.size() == 0)
            return null;
        return guests.get(0);
    }

    public Guest getGuestByUid(Long uid){
        List<Guest> guests = jdbcTemplate.query(getGuestByUid,new Object[]{uid},(rs,rowNum) -> new Guest(rs.getLong("id"),rs.getString("uid"),rs.getString("name"),rs.getString("about"),rs.getString("email"),rs.getString("contact")));
        if(guests.size() == 0)
            return null;
        return guests.get(0);
    }

    public List<Guest> getGuestsByName(String name){
        return jdbcTemplate.query(getGuestsByName,new Object[]{"%"+name+"%"},(rs,rowNum) -> new Guest(rs.getLong("id"),rs.getString("uid"),rs.getString("name"),rs.getString("about"),rs.getString("email"),rs.getString("contact")));
    }

    public void update(Guest guest){
        jdbcTemplate.update(update,guest.getUid(),guest.getName(),guest.getAbout(),guest.getEmail(),guest.getContact(),guest.getId());
    }

    public void updateByUid(Guest guest){
        jdbcTemplate.update(updateByUid,guest.getName(),guest.getAbout(),guest.getEmail(),guest.getContact(),guest.getUid());
    }

    public void save(Guest guest){
        jdbcTemplate.update(save,guest.getUid(),guest.getName(),guest.getAbout(),guest.getEmail(),guest.getContact());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByUid(Long uid){
        jdbcTemplate.update(deleteByUid, uid);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }

}
