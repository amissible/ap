package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.TypeConfigDao;
import com.amissible.appointment.model.Type;
import com.amissible.appointment.model.TypeConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeConfigDaoImpl implements TypeConfigDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_TYPE_CONFIG";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`type_id` bigint unsigned NOT NULL,`day` varchar(10) NOT NULL,`label` varchar(255) NOT NULL,`start_time` varchar(6) NOT NULL,`end_time` varchar(6) NOT NULL,`fees` float NOT NULL DEFAULT '0')";
    private final String getAllTypeConfigs = "SELECT * FROM "+tableName;
    private final String getTypeConfigById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getTypeConfigsByTypeId = "SELECT * FROM "+tableName+" WHERE type_id=?";
    private final String getTypeConfigsByDay = "SELECT * FROM "+tableName+" WHERE day=?";
    private final String getTypeConfigsByDayOfTypeId = "SELECT * FROM "+tableName+" WHERE day=? AND  type_id=?";
    private final String update = "UPDATE "+tableName+" SET type_id=?,day=?,label=?,start_time=?,end_time=?,fees=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(type_id,day,label,start_time,end_time,fees) VALUES(?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByTypeId = "DELETE FROM "+tableName+" WHERE type_id=?";
    private final String deleteByDay = "DELETE FROM "+tableName+" WHERE day=?";
    private final String deleteByDayOfTypeId = "DELETE FROM "+tableName+" WHERE day=? AND  type_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<TypeConfig> getAllTypeConfigs(){
        return jdbcTemplate.query(getAllTypeConfigs,new Object[]{},(rs,rowNum) -> new TypeConfig(rs.getLong("id"),rs.getLong("type_id"),rs.getString("day"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getFloat("fees")));
    }

    public TypeConfig getTypeConfigById(Long id){
        List<TypeConfig> typeConfigs = jdbcTemplate.query(getTypeConfigById,new Object[]{id},(rs,rowNum) -> new TypeConfig(rs.getLong("id"),rs.getLong("type_id"),rs.getString("day"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getFloat("fees")));
        if(typeConfigs.size() == 0)
            return null;
        return typeConfigs.get(0);
    }

    public List<TypeConfig> getTypeConfigsByTypeId(Long typeId){
        return jdbcTemplate.query(getTypeConfigsByTypeId,new Object[]{typeId},(rs,rowNum) -> new TypeConfig(rs.getLong("id"),rs.getLong("type_id"),rs.getString("day"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getFloat("fees")));
    }

    public List<TypeConfig> getTypeConfigsByDay(String day){
        return jdbcTemplate.query(getTypeConfigsByDay,new Object[]{day},(rs,rowNum) -> new TypeConfig(rs.getLong("id"),rs.getLong("type_id"),rs.getString("day"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getFloat("fees")));
    }

    public List<TypeConfig> getTypeConfigsByDayOfTypeId(String day,Long typeId){
        return jdbcTemplate.query(getTypeConfigsByDayOfTypeId,new Object[]{day,typeId},(rs,rowNum) -> new TypeConfig(rs.getLong("id"),rs.getLong("type_id"),rs.getString("day"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getFloat("fees")));
    }

    public void update(TypeConfig typeConfig){
        jdbcTemplate.update(update,typeConfig.getTypeId(),typeConfig.getDay(),typeConfig.getLabel(),typeConfig.getStartTime(),typeConfig.getEndTime(),typeConfig.getFees(),typeConfig.getId());
    }

    public void save(TypeConfig typeConfig){
        jdbcTemplate.update(save,typeConfig.getTypeId(),typeConfig.getDay(),typeConfig.getLabel(),typeConfig.getStartTime(),typeConfig.getEndTime(),typeConfig.getFees());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByTypeId(Long typeId){
        jdbcTemplate.update(deleteByTypeId, typeId);
    }

    public void deleteByDay(String day){
        jdbcTemplate.update(deleteByDay, day);
    }

    public void deleteByDayOfTypeId(String day,Long typeId){
        jdbcTemplate.update(deleteByDayOfTypeId, day, typeId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }

}
