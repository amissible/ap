package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.AppointmentDao;
import com.amissible.appointment.model.Appointment;
import com.amissible.appointment.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentDaoImpl implements AppointmentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_APPOINTMENT";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`host_id` bigint unsigned NOT NULL,`guest_id` bigint unsigned NOT NULL,`pay_id` bigint unsigned NOT NULL,`fees` float NOT NULL DEFAULT '0',`date` varchar(20) NOT NULL,`name` varchar(100) NOT NULL,`description` varchar(255) NOT NULL,`label` varchar(255) NOT NULL,`start_time` varchar(6) NOT NULL,`end_time` varchar(6) NOT NULL,`guest_message` varchar(255) NOT NULL,`book_time_stamp` varchar(50) NOT NULL,`status` varchar(20) NOT NULL DEFAULT 'OPEN')";
    private final String getAllAppointments = "SELECT * FROM "+tableName;
    private final String getAppointmentById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getAppointmentsByHostId = "SELECT * FROM "+tableName+" WHERE host_id=?";
    private final String getNextAppointmentsByHostId = "SELECT * FROM "+tableName+" WHERE host_id=? AND `date`>=?";
    private final String getAppointmentsByGuestId = "SELECT * FROM "+tableName+" WHERE guest_id=?";
    private final String getAppointmentsByDate = "SELECT * FROM "+tableName+" WHERE date=?";
    private final String getAppointmentsByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getAppointmentsByBookTimeStamp = "SELECT * FROM "+tableName+" WHERE book_time_stamp LIKE ?";
    private final String update = "UPDATE "+tableName+" SET host_id=?,guest_id=?,pay_id=?,fees=?,date=?,name=?,description=?,label=?,start_time=?,end_time=?,guest_message=?,book_time_stamp=?,status=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(host_id,guest_id,pay_id,fees,date,name,description,label,start_time,end_time,guest_message,book_time_stamp,status) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Appointment> getAllAppointments(){
        return jdbcTemplate.query(getAllAppointments,new Object[]{},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public Appointment getAppointmentById(Long id){
        List<Appointment> appointments = jdbcTemplate.query(getAppointmentById,new Object[]{id},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
        if(appointments.size() == 0)
            return null;
        return appointments.get(0);
    }

    public List<Appointment> getAppointmentsByHostId(Long hostId){
        return jdbcTemplate.query(getAppointmentsByHostId,new Object[]{hostId},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public List<Appointment> getNextAppointmentsByHostId(Long hostId,String date){
        return jdbcTemplate.query(getNextAppointmentsByHostId,new Object[]{hostId,date},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public List<Appointment> getAppointmentsByGuestId(Long guestId){
        return jdbcTemplate.query(getAppointmentsByGuestId,new Object[]{guestId},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public List<Appointment> getAppointmentsByDate(String date){
        return jdbcTemplate.query(getAppointmentsByDate,new Object[]{date},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public List<Appointment> getAppointmentsByStatus(String status){
        return jdbcTemplate.query(getAppointmentsByStatus,new Object[]{status},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public List<Appointment> getAppointmentsByBookTimeStamp(String bookTimeStamp){
        return jdbcTemplate.query(getAppointmentsByBookTimeStamp,new Object[]{"%"+bookTimeStamp+"%"},(rs,rowNum) -> new Appointment(rs.getLong("id"),rs.getLong("host_id"),rs.getLong("guest_id"),rs.getLong("pay_id"),rs.getFloat("fees"),rs.getString("date"),rs.getString("name"),rs.getString("description"),rs.getString("label"),rs.getString("start_time"),rs.getString("end_time"),rs.getString("guest_message"),rs.getString("book_time_stamp"),rs.getString("status")));
    }

    public void update(Appointment appointment){
        jdbcTemplate.update(update,appointment.getHostId(),appointment.getGuestId(),appointment.getPayId(),appointment.getFees(),appointment.getDate(),appointment.getName(),appointment.getDescription(),appointment.getLabel(),appointment.getStartTime(),appointment.getEndTime(),appointment.getGuestMessage(),appointment.getBookTimeStamp(),appointment.getStatus(),appointment.getId());
    }

    public void save(Appointment appointment){
        jdbcTemplate.update(save,appointment.getHostId(),appointment.getGuestId(),appointment.getPayId(),appointment.getFees(),appointment.getDate(),appointment.getName(),appointment.getDescription(),appointment.getLabel(),appointment.getStartTime(),appointment.getEndTime(),appointment.getGuestMessage(),appointment.getBookTimeStamp(),appointment.getStatus());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
