package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.HostDao;
import com.amissible.appointment.model.Guest;
import com.amissible.appointment.model.Host;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HostDaoImpl implements HostDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_HOST";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`type_id` bigint unsigned NOT NULL,`emp_id` varchar(25) NOT NULL,`name` varchar(100) NOT NULL,`designation` varchar(100) NOT NULL,`about` varchar(255) NOT NULL,`email` varchar(50) NOT NULL,`contact` varchar(20) NOT NULL,`fees` float NOT NULL DEFAULT '0',`lead_days` int NOT NULL DEFAULT '0',`status` varchar(20) NOT NULL DEFAULT 'ACTIVE')";
    private final String getAllHosts = "SELECT * FROM "+tableName;
    private final String getHostById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getHostByEmpId = "SELECT * FROM "+tableName+" WHERE emp_id=?";
    private final String getHostsByTypeId = "SELECT * FROM "+tableName+" WHERE type_id=?";
    private final String getHostsByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getHostsByName = "SELECT * FROM "+tableName+" WHERE name LIKE ?";
    private final String update = "UPDATE "+tableName+" SET type_id=?,emp_id=?,name=?,designation=?,about=?,email=?,contact=?,fees=?,lead_days=?,status=? WHERE id=?";
    private final String updateByEmpId = "UPDATE "+tableName+" SET type_id=?,name=?,designation=?,about=?,email=?,contact=?,fees=?,lead_days=?,status=? WHERE emp_id=?";
    private final String save = "INSERT INTO "+tableName+"(type_id,emp_id,name,designation,about,email,contact,fees,lead_days,status) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByEmpId = "DELETE FROM "+tableName+" WHERE emp_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Host> getAllHosts(){
        return jdbcTemplate.query(getAllHosts,new Object[]{},(rs,rowNum) -> new Host(rs.getLong("id"),rs.getLong("type_id"),rs.getString("emp_id"),rs.getString("name"),rs.getString("designation"),rs.getString("about"),rs.getString("email"),rs.getString("contact"),rs.getFloat("fees"),rs.getInt("lead_days"),rs.getString("status")));
    }

    public Host getHostById(Long id){
        List<Host> hosts = jdbcTemplate.query(getHostById,new Object[]{id},(rs,rowNum) -> new Host(rs.getLong("id"),rs.getLong("type_id"),rs.getString("emp_id"),rs.getString("name"),rs.getString("designation"),rs.getString("about"),rs.getString("email"),rs.getString("contact"),rs.getFloat("fees"),rs.getInt("lead_days"),rs.getString("status")));
        if(hosts.size() == 0)
            return null;
        return hosts.get(0);
    }

    public Host getHostByEmpId(String empId){
        List<Host> hosts = jdbcTemplate.query(getHostByEmpId,new Object[]{empId},(rs,rowNum) -> new Host(rs.getLong("id"),rs.getLong("type_id"),rs.getString("emp_id"),rs.getString("name"),rs.getString("designation"),rs.getString("about"),rs.getString("email"),rs.getString("contact"),rs.getFloat("fees"),rs.getInt("lead_days"),rs.getString("status")));
        if(hosts.size() == 0)
            return null;
        return hosts.get(0);
    }

    public List<Host> getHostsByTypeId(Long typeId){
        return jdbcTemplate.query(getHostsByTypeId,new Object[]{typeId},(rs,rowNum) -> new Host(rs.getLong("id"),rs.getLong("type_id"),rs.getString("emp_id"),rs.getString("name"),rs.getString("designation"),rs.getString("about"),rs.getString("email"),rs.getString("contact"),rs.getFloat("fees"),rs.getInt("lead_days"),rs.getString("status")));
    }

    public List<Host> getHostsByStatus(String status){
        return jdbcTemplate.query(getHostsByStatus,new Object[]{status},(rs,rowNum) -> new Host(rs.getLong("id"),rs.getLong("type_id"),rs.getString("emp_id"),rs.getString("name"),rs.getString("designation"),rs.getString("about"),rs.getString("email"),rs.getString("contact"),rs.getFloat("fees"),rs.getInt("lead_days"),rs.getString("status")));
    }

    public List<Host> getHostsByName(String name){
        return jdbcTemplate.query(getHostsByName,new Object[]{"%"+name+"%"},(rs,rowNum) -> new Host(rs.getLong("id"),rs.getLong("type_id"),rs.getString("emp_id"),rs.getString("name"),rs.getString("designation"),rs.getString("about"),rs.getString("email"),rs.getString("contact"),rs.getFloat("fees"),rs.getInt("lead_days"),rs.getString("status")));
    }

    public void update(Host host){
        jdbcTemplate.update(update,host.getTypeId(),host.getEmpId(),host.getName(),host.getDesignation(),host.getAbout(),host.getEmail(),host.getContact(),host.getFees(),host.getLeadDays(),host.getStatus(),host.getId());
    }

    public void updateByEmpId(Host host){
        jdbcTemplate.update(updateByEmpId,host.getTypeId(),host.getName(),host.getDesignation(),host.getAbout(),host.getEmail(),host.getContact(),host.getFees(),host.getLeadDays(),host.getStatus(),host.getEmpId());
    }

    public void save(Host host){
        jdbcTemplate.update(save,new Object[]{host.getTypeId(),host.getEmpId(),host.getName(),host.getDesignation(),host.getAbout(),host.getEmail(),host.getContact(),host.getFees(),host.getLeadDays(),host.getStatus()});
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByEmpId(String empId){
        jdbcTemplate.update(deleteByEmpId, empId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }

}
