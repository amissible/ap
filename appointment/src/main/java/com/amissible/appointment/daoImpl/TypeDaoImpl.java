package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.TypeDao;
import com.amissible.appointment.model.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TypeDaoImpl implements TypeDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_TYPE";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`name` varchar(100) NOT NULL,`description` varchar(255) NOT NULL,`status` varchar(20) NOT NULL DEFAULT 'ACTIVE')";
    private final String getAllTypes = "SELECT * FROM "+tableName;
    private final String getTypeById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getTypeByName = "SELECT * FROM "+tableName+" WHERE name=?";
    private final String getTypesByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String update = "UPDATE "+tableName+" SET name=?,description=?,status=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(name,description,status) VALUES(?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Type> getAllTypes(){
        return jdbcTemplate.query(getAllTypes,new Object[]{},(rs,rowNum) -> new Type(rs.getLong("id"),rs.getString("name"),rs.getString("description"),rs.getString("status")));
    }

    public Type getTypeById(Long id){
        List<Type> types = jdbcTemplate.query(getTypeById,new Object[]{id},(rs,rowNum) -> new Type(rs.getLong("id"),rs.getString("name"),rs.getString("description"),rs.getString("status")));
        if(types.size() == 0)
            return null;
        return types.get(0);
    }

    public Type getTypeByName(String name){
        List<Type> types = jdbcTemplate.query(getTypeByName,new Object[]{name},(rs,rowNum) -> new Type(rs.getLong("id"),rs.getString("name"),rs.getString("description"),rs.getString("status")));
        if(types.size() == 0)
            return null;
        return types.get(0);
    }

    public List<Type> getTypesByStatus(String status){
        return jdbcTemplate.query(getTypesByStatus,new Object[]{status},(rs,rowNum) -> new Type(rs.getLong("id"),rs.getString("name"),rs.getString("description"),rs.getString("status")));
    }

    public void update(Type type){
        jdbcTemplate.update(update, type.getName(),type.getDescription(),type.getStatus(),type.getId());
    }

    public void save(Type type){
        jdbcTemplate.update(save, type.getName(),type.getDescription(),type.getStatus());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }

}
