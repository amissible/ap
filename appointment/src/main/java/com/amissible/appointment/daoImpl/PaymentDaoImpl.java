package com.amissible.appointment.daoImpl;

import com.amissible.appointment.dao.PaymentDao;
import com.amissible.appointment.model.Host;
import com.amissible.appointment.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentDaoImpl implements PaymentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_AM_PAYMENT";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`guest_id` bigint unsigned NOT NULL,`appointment_id` bigint unsigned NOT NULL,`txn_id` varchar(50) NOT NULL,`txn_amount` float NOT NULL DEFAULT '0',`txn_status` varchar(20) NOT NULL DEFAULT 'FLAG_UP',`txn_time_stamp` varchar(50) NOT NULL)";
    private final String getAllPayments = "SELECT * FROM "+tableName;
    private final String getPaymentById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getPaymentsByGuestId = "SELECT * FROM "+tableName+" WHERE guest_id=?";
    private final String getPaymentsByAppointmentId = "SELECT * FROM "+tableName+" WHERE appointment_id=?";
    private final String getSuccessPaymentByAppointmentId = "SELECT * FROM "+tableName+" WHERE appointment_id=? AND txn_status='SUCCESS'";
    private final String getPaymentByTransactionId = "SELECT * FROM "+tableName+" WHERE txn_id=?";
    private final String getPaymentsByTransactionStatus = "SELECT * FROM "+tableName+" WHERE txn_status=?";
    private final String getPaymentsByTransactionTimeStamp = "SELECT * FROM "+tableName+" WHERE txn_time_stamp LIKE ?";
    private final String update = "UPDATE "+tableName+" SET guest_id=?,appointment_id=?,txn_id=?,txn_amount=?,txn_status=?,txn_time_stamp=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(guest_id,appointment_id,txn_id,txn_amount,txn_status,txn_time_stamp) VALUES(?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Payment> getAllPayments(){
        return jdbcTemplate.query(getAllPayments,new Object[]{},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public Payment getPaymentById(Long id){
        List<Payment> payments = jdbcTemplate.query(getPaymentById,new Object[]{id},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
        if(payments.size() == 0)
            return null;
        return payments.get(0);
    }

    public List<Payment> getPaymentsByGuestId(Long guestId){
        return jdbcTemplate.query(getPaymentsByGuestId,new Object[]{guestId},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public List<Payment> getPaymentsByAppointmentId(Long appointmentId){
        return jdbcTemplate.query(getPaymentsByAppointmentId,new Object[]{appointmentId},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public Payment getSuccessPaymentByAppointmentId(Long appointmentId){
        List<Payment> payments = jdbcTemplate.query(getSuccessPaymentByAppointmentId,new Object[]{appointmentId},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
        if(payments.size() == 0)
            return null;
        return payments.get(0);
    }

    public Payment getPaymentByTransactionId(String txnId){
        List<Payment> payments = jdbcTemplate.query(getPaymentByTransactionId,new Object[]{txnId},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
        if(payments.size() == 0)
            return null;
        return payments.get(0);
    }

    public List<Payment> getPaymentsByTransactionStatus(String txnStatus){
        return jdbcTemplate.query(getPaymentsByTransactionStatus,new Object[]{txnStatus},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public List<Payment> getPaymentsByTransactionTimeStamp(String txnTimeStamp){
        return jdbcTemplate.query(getPaymentsByTransactionTimeStamp,new Object[]{"%"+txnTimeStamp+"%"},(rs,rowNum) -> new Payment(rs.getLong("id"),rs.getLong("guest_id"),rs.getLong("appointment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public void update(Payment payment){
        jdbcTemplate.update(update,payment.getGuestId(),payment.getAppointmentId(),payment.getTxnId(),payment.getTxnAmount(),payment.getTxnStatus(),payment.getTxnTimeStamp(),payment.getId());
    }

    public void save(Payment payment){
        jdbcTemplate.update(save,payment.getGuestId(),payment.getAppointmentId(),payment.getTxnId(),payment.getTxnAmount(),payment.getTxnStatus(),payment.getTxnTimeStamp());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
