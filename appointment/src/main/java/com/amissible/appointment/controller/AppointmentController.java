package com.amissible.appointment.controller;

import com.amissible.appointment.model.*;
import com.amissible.appointment.payload.AppointmentSlot;
import com.amissible.appointment.service.AppointmentService;
import com.amissible.appointment.service.EmailService;
import com.amissible.appointment.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    AppointmentService service;

    @GetMapping("/start")
    public void start(){
        service.start();
    }

    @GetMapping("/destroy")
    public void destroy(){
        service.destroy();
    }

    @PostMapping("/host/register")
    public void registerHost(@RequestBody Host host){
        service.registerHost(host);
    }

    @PostMapping("/host/update")
    public void updateHost(@RequestBody Host host){
        service.updateHost(host);
    }

    @GetMapping("/host/getActiveHosts")
    public List<Host> getActiveHosts(){
        return service.getActiveHosts();
    }

    @PostMapping("/guest/register")
    public void registerGuest(@RequestBody Guest guest){
        service.registerGuest(guest);
    }

    @PostMapping("/guest/update")
    public void updateGuest(@RequestBody Guest guest){
        service.updateGuest(guest);
    }

    @PostMapping("/type/add")
    public void addType(@RequestBody Type type){
        service.addType(type);
    }

    @PostMapping("/type/update")
    public void updateType(@RequestBody Type type){
        service.updateType(type);
    }

    @PostMapping("/typeConfig/add")
    public void addTypeConfig(@RequestBody TypeConfig[] typeConfig){
        service.addTypeConfig(typeConfig);
    }

    @PostMapping("/typeConfig/update")
    public void updateTypeConfig(@RequestBody TypeConfig typeConfig){
        service.updateTypeConfig(typeConfig);
    }

    @PostMapping("/holiday/add")
    public void addHoliday(@RequestBody Holiday holiday){
        service.addHoliday(holiday);
    }

    @PostMapping("/holiday/update")
    public void updateHoliday(@RequestBody Holiday holiday){
        service.updateHoliday(holiday);
    }


    @GetMapping("/getAppointmentsInfo")
    public Map<String, List<AppointmentSlot>> getAppointmentsInfo(@RequestParam Long hostId){
        return service.getAppointmentsInfo(hostId);
    }

    @RequestMapping("/book")
    //@PostMapping("/book")
    public RedirectView book(@RequestParam Long appointmentId, @RequestParam Long guestId, @RequestParam String guestMessage){
        return service.bookAppointment(appointmentId,guestId,guestMessage);
    }

    @RequestMapping("/success")
    public RedirectView successBook(@RequestParam String token,@RequestParam String state,@RequestParam Float amount,@RequestParam String paymentId){
        Integer pos = token.indexOf("-");
        return service.confirmAppointment(Long.parseLong(token.substring(0,pos)),Long.parseLong(token.substring(pos+1)),state,amount,paymentId);
    }

    @RequestMapping("/cancel")
    public RedirectView cancelBook(@RequestParam String token){
        Integer pos = token.indexOf("-");
        return service.clearAppointment(Long.parseLong(token.substring(0,pos)),Long.parseLong(token.substring(pos+1)));
    }




    @RequestMapping("/testConfirm")
    public void testConfirm(){
        service.confirmAppointment(1L,10L,"completed",300f,"PAY-123456");
    }

    @RequestMapping("/test")
    public void test1(){
        Utility.generateICS(1L,"TestEvent","Dvent Desc","Gorakhpur","Dr. Gaurav","osg.indian@gmail.com","Omendra","omendrasinghgakkhar1996@gmail.com","Asia/Kolkata","2021/01/28","17:25","17:30","/app/export/appointment/appointment1.ics");
    }

    @Autowired
    EmailService emailService;

    @RequestMapping("/emtest")
    public void testEm(){
        Mail mail = new Mail();
        mail.setTo("omendrasinghgakkhar1996@gmail.com");
        mail.setSubject("Test Mail");
        mail.setTemplate("emailtest.ftl");
        mail.setAttachments(new String[]{"/app/export/appointment/appointment1.ics"});
        Map<String,String> map = new HashMap<>();
        map.put("name","Omendra");
        mail.setModel(map);
        emailService.sendSimpleMessage(mail);
    }


    //TEST
/*
    @Autowired
    TypeDao typeDao;

    @RequestMapping("/test1")
    public void test1(){
        typeDao.deleteAll();
    }

    @RequestMapping("/test2")
    public List<Type> test2(){
        return typeDao.getAllTypes();
    }

    @RequestMapping("/test3")
    public void test3(){
        typeDao.save(new Type("Doctor Appointment","Personal Appointment to Doctor","ACTIVE"));
        typeDao.save(new Type("Nurse Appointment","Personal Appointment to Nurse","ACTIVE"));
        typeDao.save(new Type("Pathology Appointment","Personal Appointment to Pathology","INACTIVE"));
    }

    public void TableType(){
        typeDao.save(new Type("Doctor Appointment","Personal Appointment to Doctor","ACTIVE"));
        typeDao.save(new Type("Nurse Appointment","Personal Appointment to Nurse","ACTIVE"));
        typeDao.save(new Type("Pathology Appointment","Personal Appointment to Pathology","INACTIVE"));
    }*/
}
