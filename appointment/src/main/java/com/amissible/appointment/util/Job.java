package com.amissible.appointment.util;

import com.amissible.appointment.service.AppointmentService;
import com.amissible.appointment.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Job extends Thread{

    private Long lap;
    private Boolean status;
    private Integer counter;
    private Integer maxCounter;

    @Autowired
    JobService jobService;

    public Job() {
        lap = 1000L * 10; // 10 seconds
        status = true;
        counter = 0;
        maxCounter = 24;
    }

    @Override
    public void run() {
        super.run();
        taskGroup0();
        try{
            do{
                counter++;
                switch (counter) {
                    case 1 -> taskGroup1(); case 2 -> taskGroup2(); case 3 -> taskGroup3();
                    case 4 -> taskGroup4(); case 5 -> taskGroup5(); case 6 -> taskGroup6();
                    case 7 -> taskGroup7(); case 8 -> taskGroup8(); case 9 -> taskGroup9();
                    case 10 -> taskGroup10(); case 11 -> taskGroup11(); case 12 -> taskGroup12();
                    case 13 -> taskGroup13(); case 14 -> taskGroup14(); case 15 -> taskGroup15();
                    case 16 -> taskGroup16(); case 17 -> taskGroup17(); case 18 -> taskGroup18();
                    case 19 -> taskGroup19(); case 20 -> taskGroup20(); case 21 -> taskGroup21();
                    case 22 -> taskGroup22(); case 23 -> taskGroup23(); case 24 -> taskGroup24();
                }
                if(counter == maxCounter)
                    counter = 0;
                sleep(lap); // Calculate Lap time with Local Time
            }while(status);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void taskGroup0() {
        System.out.println("Triggered Task group 0");
        jobService.createTables();
    }
    private void taskGroup1() {
        System.out.println("Triggered Task group 1");
        jobService.generateAppointmentSlots();

    }
    private void taskGroup2() { System.out.println("Triggered Task group 2"); }
    private void taskGroup3() { System.out.println("Triggered Task group 3"); }
    private void taskGroup4() { System.out.println("Triggered Task group 4"); }
    private void taskGroup5() { System.out.println("Triggered Task group 5"); }
    private void taskGroup6() { System.out.println("Triggered Task group 6"); }
    private void taskGroup7() { System.out.println("Triggered Task group 7"); }
    private void taskGroup8() { System.out.println("Triggered Task group 8"); }
    private void taskGroup9() { System.out.println("Triggered Task group 9"); }
    private void taskGroup10() { System.out.println("Triggered Task group 10"); }
    private void taskGroup11() { System.out.println("Triggered Task group 11"); }
    private void taskGroup12() { System.out.println("Triggered Task group 12"); }
    private void taskGroup13() { System.out.println("Triggered Task group 13"); }
    private void taskGroup14() { System.out.println("Triggered Task group 14"); }
    private void taskGroup15() { System.out.println("Triggered Task group 15"); }
    private void taskGroup16() { System.out.println("Triggered Task group 16"); }
    private void taskGroup17() { System.out.println("Triggered Task group 17"); }
    private void taskGroup18() { System.out.println("Triggered Task group 18"); }
    private void taskGroup19() { System.out.println("Triggered Task group 19"); }
    private void taskGroup20() { System.out.println("Triggered Task group 20"); }
    private void taskGroup21() { System.out.println("Triggered Task group 21"); }
    private void taskGroup22() { System.out.println("Triggered Task group 22"); }
    private void taskGroup23() { System.out.println("Triggered Task group 23"); }
    private void taskGroup24() { System.out.println("Triggered Task group 24"); }

    public Long getLap() {
        return lap;
    }

    public void setLap(Long lap) {
        this.lap = lap;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public Integer getMaxCounter() {
        return maxCounter;
    }

    public void setMaxCounter(Integer maxCounter) {
        this.maxCounter = maxCounter;
    }
}
