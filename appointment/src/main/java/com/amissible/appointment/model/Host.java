package com.amissible.appointment.model;

public class Host {

    private Long id;
    private Long typeId;
    private String empId;
    private String name;
    private String designation;
    private String about;
    private String email;
    private String contact;
    private Float fees;
    private Integer leadDays;
    private String status;

    public Host() {
        id = 0L;
        typeId = 0L;
        empId = "";
        name = "";
        designation = "";
        about = "";
        email = "";
        contact = "";
        fees = 0F;
        leadDays = 0;
        status = "ACTIVE";
    }

    public Host(Long typeId, String empId, String name, String designation, String about, String email, String contact, Float fees, Integer leadDays, String status) {
        this.typeId = typeId;
        this.empId = empId;
        this.name = name;
        this.designation = designation;
        this.about = about;
        this.email = email;
        this.contact = contact;
        this.fees = fees;
        this.leadDays = leadDays;
        this.status = status;
    }

    public Host(Long id, Long typeId, String empId, String name, String designation, String about, String email, String contact, Float fees, Integer leadDays, String status) {
        this.id = id;
        this.typeId = typeId;
        this.empId = empId;
        this.name = name;
        this.designation = designation;
        this.about = about;
        this.email = email;
        this.contact = contact;
        this.fees = fees;
        this.leadDays = leadDays;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Float getFees() {
        return fees;
    }

    public void setFees(Float fees) {
        this.fees = fees;
    }

    public Integer getLeadDays() {
        return leadDays;
    }

    public void setLeadDays(Integer leadDays) {
        this.leadDays = leadDays;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Host{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", empId='" + empId + '\'' +
                ", name='" + name + '\'' +
                ", designation='" + designation + '\'' +
                ", about='" + about + '\'' +
                ", email='" + email + '\'' +
                ", contact='" + contact + '\'' +
                ", fees=" + fees +
                ", leadDays=" + leadDays +
                ", status='" + status + '\'' +
                '}';
    }
}
