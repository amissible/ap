package com.amissible.appointment.model;

public class Appointment {

    private Long id;
    private Long hostId;
    private Long guestId;
    private Long payId;
    private Float fees;
    private String date;
    private String name;
    private String description;
    private String label;
    private String startTime; //HH:MM
    private String endTime; //HH:MM
    private String guestMessage;
    private String bookTimeStamp;
    private String status;

    public Appointment() {
        id = hostId = guestId = payId =0L;
        fees = 0F;
        date = name = description = label = startTime = endTime = guestMessage = bookTimeStamp = status = "";
    }

    public Appointment(Long hostId, Long guestId, Long payId, Float fees, String date, String name, String description, String label, String startTime, String endTime, String guestMessage, String bookTimeStamp, String status) {
        this.hostId = hostId;
        this.guestId = guestId;
        this.payId = payId;
        this.fees = fees;
        this.date = date;
        this.name = name;
        this.description = description;
        this.label = label;
        this.startTime = startTime;
        this.endTime = endTime;
        this.guestMessage = guestMessage;
        this.bookTimeStamp = bookTimeStamp;
        this.status = status;
    }

    public Appointment(Long id, Long hostId, Long guestId, Long payId, Float fees, String date, String name, String description, String label, String startTime, String endTime, String guestMessage, String bookTimeStamp, String status) {
        this.id = id;
        this.hostId = hostId;
        this.guestId = guestId;
        this.payId = payId;
        this.fees = fees;
        this.date = date;
        this.name = name;
        this.description = description;
        this.label = label;
        this.startTime = startTime;
        this.endTime = endTime;
        this.guestMessage = guestMessage;
        this.bookTimeStamp = bookTimeStamp;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHostId() {
        return hostId;
    }

    public void setHostId(Long hostId) {
        this.hostId = hostId;
    }

    public Long getGuestId() {
        return guestId;
    }

    public void setGuestId(Long guestId) {
        this.guestId = guestId;
    }

    public Long getPayId() {
        return payId;
    }

    public void setPayId(Long payId) {
        this.payId = payId;
    }

    public Float getFees() {
        return fees;
    }

    public void setFees(Float fees) {
        this.fees = fees;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getGuestMessage() {
        return guestMessage;
    }

    public void setGuestMessage(String guestMessage) {
        this.guestMessage = guestMessage;
    }

    public String getBookTimeStamp() {
        return bookTimeStamp;
    }

    public void setBookTimeStamp(String bookTimeStamp) {
        this.bookTimeStamp = bookTimeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", hostId=" + hostId +
                ", guestId=" + guestId +
                ", payId=" + payId +
                ", fees=" + fees +
                ", date='" + date + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", label='" + label + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", guestMessage='" + guestMessage + '\'' +
                ", bookTimeStamp='" + bookTimeStamp + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
