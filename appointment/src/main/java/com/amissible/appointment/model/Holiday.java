package com.amissible.appointment.model;

public class Holiday {

    Long id;
    String date; // yyyy/MM/dd
    String name;

    public Holiday() {
    }

    public Holiday(String date, String name) {
        this.date = date;
        this.name = name;
    }

    public Holiday(Long id, String date, String name) {
        this.id = id;
        this.date = date;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
