package com.amissible.appointment.model;

public class Payment {

    private Long id;
    private Long guestId;
    private Long appointmentId;
    private String txnId;
    private Float txnAmount;
    private String txnStatus;
    private String txnTimeStamp;

    public Payment() {
    }

    public Payment(Long guestId, Long appointmentId, String txnId, Float txnAmount, String txnStatus, String txnTimeStamp) {
        this.guestId = guestId;
        this.appointmentId = appointmentId;
        this.txnId = txnId;
        this.txnAmount = txnAmount;
        this.txnStatus = txnStatus;
        this.txnTimeStamp = txnTimeStamp;
    }

    public Payment(Long id, Long guestId, Long appointmentId, String txnId, Float txnAmount, String txnStatus, String txnTimeStamp) {
        this.id = id;
        this.guestId = guestId;
        this.appointmentId = appointmentId;
        this.txnId = txnId;
        this.txnAmount = txnAmount;
        this.txnStatus = txnStatus;
        this.txnTimeStamp = txnTimeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGuestId() {
        return guestId;
    }

    public void setGuestId(Long guestId) {
        this.guestId = guestId;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public Float getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(Float txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnTimeStamp() {
        return txnTimeStamp;
    }

    public void setTxnTimeStamp(String txnTimeStamp) {
        this.txnTimeStamp = txnTimeStamp;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", guestId=" + guestId +
                ", appointmentId=" + appointmentId +
                ", txnId='" + txnId + '\'' +
                ", txnAmount=" + txnAmount +
                ", txnStatus='" + txnStatus + '\'' +
                ", txnTimeStamp='" + txnTimeStamp + '\'' +
                '}';
    }
}
