package com.amissible.appointment.model;

public class TypeConfig {

    private Long id;
    private Long typeId;
    private String day;
    private String label;
    private String startTime; // HH:MM
    private String endTime; // HH:MM
    private Float fees;

    public TypeConfig() {
        id = 0L;
        typeId = 0L;
        day = "";
        label = "";
        startTime = "";
        endTime = "";
        fees = 0F;
    }

    public TypeConfig(Long typeId, String day, String label, String startTime, String endTime, Float fees) {
        this.typeId = typeId;
        this.day = day;
        this.label = label;
        this.startTime = startTime;
        this.endTime = endTime;
        this.fees = fees;
    }

    public TypeConfig(Long id, Long typeId, String day, String label, String startTime, String endTime, Float fees) {
        this.id = id;
        this.typeId = typeId;
        this.day = day;
        this.label = label;
        this.startTime = startTime;
        this.endTime = endTime;
        this.fees = fees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Float getFees() {
        return fees;
    }

    public void setFees(Float fees) {
        this.fees = fees;
    }

    @Override
    public String toString() {
        return "TypeConfig{" +
                "id=" + id +
                ", typeId=" + typeId +
                ", day='" + day + '\'' +
                ", label='" + label + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", fees=" + fees +
                '}';
    }
}
