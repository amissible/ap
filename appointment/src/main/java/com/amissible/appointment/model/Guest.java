package com.amissible.appointment.model;

public class Guest {

    private Long id;
    private String uid;
    private String name;
    private String about;
    private String email;
    private String contact;

    public Guest() {
        id = 0L;
        uid = "";
        name = "";
        about = "";
        email = "";
        contact = "";
    }

    public Guest(String uid, String name, String about, String email, String contact) {
        this.uid = uid;
        this.name = name;
        this.about = about;
        this.email = email;
        this.contact = contact;
    }

    public Guest(Long id, String uid, String name, String about, String email, String contact) {
        this.id = id;
        this.uid = uid;
        this.name = name;
        this.about = about;
        this.email = email;
        this.contact = contact;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "Guest{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", name='" + name + '\'' +
                ", about='" + about + '\'' +
                ", email='" + email + '\'' +
                ", contact='" + contact + '\'' +
                '}';
    }
}
