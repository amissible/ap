package com.amissible.appointment.dao;

import com.amissible.appointment.model.Type;

import java.util.List;

public interface TypeDao {
    void createTable();
    List<Type> getAllTypes();
    Type getTypeById(Long id);
    Type getTypeByName(String name);
    List<Type> getTypesByStatus(String status);
    void update(Type type);
    void save(Type type);
    void delete(Long id);
    void deleteAll();
    void dropTable();
}
