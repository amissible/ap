package com.amissible.appointment.dao;

import com.amissible.appointment.model.Appointment;

import java.util.List;

public interface AppointmentDao {
    void createTable();
    List<Appointment> getAllAppointments();
    Appointment getAppointmentById(Long id);
    List<Appointment> getAppointmentsByHostId(Long hostId);
    public List<Appointment> getNextAppointmentsByHostId(Long hostId,String date);
    List<Appointment> getAppointmentsByGuestId(Long guestId);
    List<Appointment> getAppointmentsByDate(String date);
    List<Appointment> getAppointmentsByStatus(String status);
    List<Appointment> getAppointmentsByBookTimeStamp(String bookTimeStamp);
    void update(Appointment appointment);
    void save(Appointment appointment);
    void delete(Long id);
    void deleteAll();
    void dropTable();
}
