package com.amissible.appointment.dao;

import com.amissible.appointment.model.Guest;

import java.util.List;

public interface GuestDao {
    void createTable();
    List<Guest> getAllGuests();
    Guest getGuestById(Long id);
    Guest getGuestByUid(Long uid);
    List<Guest> getGuestsByName(String name);
    void update(Guest guest);
    void updateByUid(Guest guest);
    void save(Guest guest);
    void delete(Long id);
    void deleteByUid(Long uid);
    void deleteAll();
    void dropTable();
}
