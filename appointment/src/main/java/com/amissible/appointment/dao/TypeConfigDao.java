package com.amissible.appointment.dao;

import com.amissible.appointment.model.TypeConfig;

import java.util.List;

public interface TypeConfigDao {
    void createTable();
    List<TypeConfig> getAllTypeConfigs();
    TypeConfig getTypeConfigById(Long id);
    List<TypeConfig> getTypeConfigsByTypeId(Long typeId);
    List<TypeConfig> getTypeConfigsByDay(String day);
    List<TypeConfig> getTypeConfigsByDayOfTypeId(String day, Long typeId);
    void update(TypeConfig typeConfig);
    void save(TypeConfig typeConfig);
    void delete(Long id);
    void deleteByTypeId(Long typeId);
    void deleteByDay(String day);
    void deleteByDayOfTypeId(String day, Long typeId);
    void deleteAll();
    void dropTable();
}
