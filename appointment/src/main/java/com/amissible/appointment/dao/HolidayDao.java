package com.amissible.appointment.dao;

import com.amissible.appointment.model.Holiday;

import java.util.List;

public interface HolidayDao {
    void createTable();
    List<Holiday> getAllHolidays();
    List<Holiday> getAllUpcomingHolidays(String date);
    Holiday getHolidayById(Long id);
    void update(Holiday holiday);
    void save(Holiday holiday);
    void delete(Long id);
    void deleteAll();
    void dropTable();
}
