package com.amissible.appointment.dao;

import com.amissible.appointment.model.Host;

import java.util.List;

public interface HostDao {
    void createTable();
    List<Host> getAllHosts();
    Host getHostById(Long id);
    Host getHostByEmpId(String empId);
    List<Host> getHostsByTypeId(Long typeId);
    public List<Host> getHostsByStatus(String status);
    List<Host> getHostsByName(String name);
    void update(Host host);
    void updateByEmpId(Host host);
    void save(Host host);
    void delete(Long id);
    void deleteByEmpId(String empId);
    void deleteAll();
    void dropTable();
}
