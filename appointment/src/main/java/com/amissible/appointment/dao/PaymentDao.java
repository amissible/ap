package com.amissible.appointment.dao;

import com.amissible.appointment.model.Payment;

import java.util.List;

public interface PaymentDao {
    void createTable();
    List<Payment> getAllPayments();
    Payment getPaymentById(Long id);
    List<Payment> getPaymentsByGuestId(Long guestId);
    List<Payment> getPaymentsByAppointmentId(Long appointmentId);
    Payment getSuccessPaymentByAppointmentId(Long appointmentId);
    Payment getPaymentByTransactionId(String txnId);
    List<Payment> getPaymentsByTransactionStatus(String txnStatus);
    List<Payment> getPaymentsByTransactionTimeStamp(String txnTimeStamp);
    void update(Payment payment);
    void save(Payment payment);
    void delete(Long id);
    void deleteAll();
    void dropTable();
}
