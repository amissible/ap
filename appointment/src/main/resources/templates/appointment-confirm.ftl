<p>Hello ${name},</p>
<p>Your appointment is confirmed.</p>
<p><span style="color: #0000ff;"><strong>Appointment Details</strong></span></p>
<p>Date : ${date} (${start} - ${end})</p>
<p>Booking Date &amp; Time : ${bookTimeStamp}</p>
<p>Message : ${guestMessage}</p>
<p><span style="color: #0000ff; line-height: 2;"><strong>Organizer Details</strong></span></p>
<p style="line-height: 0.5;"><strong>${hostName}</strong></p>
<p style="line-height: 0.5;">${designation}</p>
<p style="line-height: 0.5;">${email}</p>
<p style="line-height: 0.5;">${about}</p>
<p><span style="color: #0000ff; line-height: 2;"><strong>Payment Details</strong></span></p>
<table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr style="background-color: green; color: white;">
<td style="width: 25%; text-align: center;">Transaction ID</td>
<td style="width: 25%; text-align: center;">Amount</td>
<td style="width: 25%; text-align: center;">Status</td>
<td style="width: 25%; text-align: center;">Date &amp; Time</td>
</tr>
<tr>
<td style="width: 25%; text-align: center;">${txnId}</td>
<td style="width: 25%; text-align: center;">${txnAmount}</td>
<td style="width: 25%; text-align: center;">${txnStatus}</td>
<td style="width: 25%; text-align: center;">${txnTimeStamp}</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p style="font-size: 10px; text-align: center;">This is system generated mail. Please do not reply.</p>