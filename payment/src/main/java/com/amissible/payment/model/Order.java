package com.amissible.payment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Order {

	private String alias;
	private double price;
	private String currency;
	private String description;
	private String method;
	private String intent;

}
