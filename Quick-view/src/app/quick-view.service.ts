import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Data{
  public product_name:string; 
  image_path:string; 
  info: {
    info_head: string,
    info_body: string}
  };

  @Injectable({
  providedIn: 'root'
})
export class QuickViewService {

  constructor(private http:HttpClient ) { }
  getData()
  {
    let url="http://www.amissible.com:8082/static/quickview/blue-sanitizer-bracelet";
    return this.http.get<Data>(url);
  }
}
