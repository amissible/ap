package com.amissible.dataMigration.daoImpl;

import com.amissible.dataMigration.dao.ProcessDao;
import com.amissible.dataMigration.model.Connection;
import com.amissible.dataMigration.model.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ProcessDaoImpl implements ProcessDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_DM_PROCESS";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`name` varchar(255) NOT NULL,`user_id` bigint unsigned,`source_connection_id` bigint unsigned,`source_schema` varchar(255) NOT NULL,`source_table` varchar(255) NOT NULL,`destination_connection_id` bigint unsigned,`destination_schema` varchar(255) NOT NULL,`destination_table` varchar(255) NOT NULL,`page_size` int unsigned,`time_stamp` varchar(20) NOT NULL,`headers` TEXT NOT NULL,`types` TEXT NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByName = "SELECT * FROM "+tableName+" WHERE name=?";
    private final String update = "UPDATE "+tableName+" SET name=?,user_id=?,source_connection_id=?,source_schema=?,source_table=?,destination_connection_id=?,destination_schema=?,destination_table=?,page_size=?,time_stamp=?,headers=?,types=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(name,user_id,source_connection_id,source_schema,source_table,destination_connection_id,destination_schema,destination_table,page_size,time_stamp,headers,types) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Process> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Process(rs.getLong("id"),rs.getString("name"),rs.getLong("user_id"),rs.getLong("source_connection_id"),rs.getString("source_schema"),rs.getString("source_table"),rs.getLong("destination_connection_id"),rs.getString("destination_schema"),rs.getString("destination_table"),rs.getLong("page_size"),rs.getString("time_stamp"),fromString(rs.getString("headers")),fromString(rs.getString("types"))));
    }

    public Process getById(Long id){
        List<Process> processes = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Process(rs.getLong("id"),rs.getString("name"),rs.getLong("user_id"),rs.getLong("source_connection_id"),rs.getString("source_schema"),rs.getString("source_table"),rs.getLong("destination_connection_id"),rs.getString("destination_schema"),rs.getString("destination_table"),rs.getLong("page_size"),rs.getString("time_stamp"),fromString(rs.getString("headers")),fromString(rs.getString("types"))));
        if(processes.size() == 0)
            return null;
        return processes.get(0);
    }

    public Process getByName(String name){
        List<Process> processes = jdbcTemplate.query(getByName,new Object[]{name},(rs, rowNum) -> new Process(rs.getLong("id"),rs.getString("name"),rs.getLong("user_id"),rs.getLong("source_connection_id"),rs.getString("source_schema"),rs.getString("source_table"),rs.getLong("destination_connection_id"),rs.getString("destination_schema"),rs.getString("destination_table"),rs.getLong("page_size"),rs.getString("time_stamp"),fromString(rs.getString("headers")),fromString(rs.getString("types"))));
        if(processes.size() == 0)
            return null;
        return processes.get(0);
    }

    public void update(Process process){
        jdbcTemplate.update(update,process.getName(),process.getUserId(),process.getSourceConnectionId(),process.getSourceSchema(),process.getSourceTable(),process.getDestinationConnectionId(),process.getDestinationSchema(),process.getDestinationTable(),process.getPageSize(),process.getTimeStamp(),Arrays.toString(process.getHeaders()),Arrays.toString(process.getTypes()),process.getId());
    }

    public void save(Process process){
        jdbcTemplate.update(save,process.getName(),process.getUserId(),process.getSourceConnectionId(),process.getSourceSchema(),process.getSourceTable(),process.getDestinationConnectionId(),process.getDestinationSchema(),process.getDestinationTable(),process.getPageSize(),process.getTimeStamp(),Arrays.toString(process.getHeaders()),Arrays.toString(process.getTypes()));
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }

    private String[] fromString(String string) {
        return string.replace("[", "").replace("]", "").split(", ");
    }
}
