package com.amissible.dataMigration.daoImpl;

import com.amissible.dataMigration.dao.ConnectionDao;
import com.amissible.dataMigration.model.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConnectionDaoImpl implements ConnectionDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "AM_DM_CONNECTION";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`name` varchar(255) NOT NULL,`url` varchar(255) NOT NULL,`driver` varchar(100) NOT NULL,`username` varchar(255) NOT NULL,`password` varchar(255) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByName = "SELECT * FROM "+tableName+" WHERE name=?";
    private final String update = "UPDATE "+tableName+" SET name=?,url=?,driver=?,username=?,password=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(name,url,driver,username,password) VALUES(?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Connection> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Connection(rs.getLong("id"),rs.getString("name"),rs.getString("url"),rs.getString("driver"),rs.getString("username"),rs.getString("password")));
    }

    public Connection getById(Long id){
        List<Connection> users = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Connection(rs.getLong("id"),rs.getString("name"),rs.getString("url"),rs.getString("driver"),rs.getString("username"),rs.getString("password")));
        if(users.size() == 0)
            return null;
        return users.get(0);
    }

    public Connection getByName(String name){
        List<Connection> users = jdbcTemplate.query(getByName,new Object[]{name},(rs, rowNum) -> new Connection(rs.getLong("id"),rs.getString("name"),rs.getString("url"),rs.getString("driver"),rs.getString("username"),rs.getString("password")));
        if(users.size() == 0)
            return null;
        return users.get(0);
    }

    public void update(Connection connection){
        jdbcTemplate.update(update,connection.getName(),connection.getUrl(),connection.getDriver(),connection.getUsername(),connection.getPassword(),connection.getId());
    }

    public void save(Connection connection){
        jdbcTemplate.update(save,connection.getName(),connection.getUrl(),connection.getDriver(),connection.getUsername(),connection.getPassword());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
