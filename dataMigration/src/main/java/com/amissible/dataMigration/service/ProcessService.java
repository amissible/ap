package com.amissible.dataMigration.service;

import com.amissible.dataMigration.dao.ProcessDao;
import com.amissible.dataMigration.exception.AppException;
import com.amissible.dataMigration.model.Connection;
import com.amissible.dataMigration.model.Process;
import com.amissible.dataMigration.model.ProcessJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

@Service
public class ProcessService {

    @Autowired
    private ConnectionService connectionService;

    @Autowired
    private ProcessDao processDao;

    @Autowired
    private Map<Long, ProcessJob> processMap;

    @Autowired
    private BlockingQueue<String> deletionQueue;

    public List<Process> getAllProcesses(){
        return processDao.getAll();
    }

    public Process getProcess(Long processId){
        return processDao.getById(processId);
    }

    public Process saveProcess(Process process){
        Process process1 = processDao.getByName(process.getName());
        if(process1 != null)
            throw new AppException("Process with same name is already available", HttpStatus.CONFLICT);
        processDao.save(process);
        process1 = processDao.getByName(process.getName());
        if(process1 == null)
            throw new AppException("Process could not be saved", HttpStatus.INTERNAL_SERVER_ERROR);
        ProcessJob processJob = new ProcessJob(process1,connectionService.getConnectionJdbc(process1.getSourceConnectionId()),connectionService.getConnectionJdbc(process1.getDestinationConnectionId()));
        processMap.put(process1.getId(),processJob);
        return process1;
    }

    public void deleteProcess(Long processId){
        ProcessJob processJob = processMap.get(processId);
        if(processJob == null)
            throw new AppException("Process not found",HttpStatus.NOT_FOUND);
        processJob.terminate();
        processMap.remove(processId);
        processDao.delete(processId);
    }

    public Boolean startProcess(Long processId){
        ProcessJob processJob = processMap.get(processId);
        if(processJob == null)
            throw new AppException("Process not found",HttpStatus.NOT_FOUND);
        return processJob.initiate();
    }

    public Boolean killProcess(Long processId){
        ProcessJob processJob = processMap.get(processId);
        if(processJob == null)
            throw new AppException("Process not found",HttpStatus.NOT_FOUND);
        return processJob.terminate();
    }

    public void pauseProcess(Long processId){
        ProcessJob processJob = processMap.get(processId);
        if(processJob == null)
            throw new AppException("Process not found",HttpStatus.NOT_FOUND);
        processJob.pause();
    }

    public void playProcess(Long processId){
        ProcessJob processJob = processMap.get(processId);
        if(processJob == null)
            throw new AppException("Process not found",HttpStatus.NOT_FOUND);
        processJob.play();
    }
}
