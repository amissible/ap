package com.amissible.dataMigration.service;

import com.amissible.dataMigration.dao.ConnectionDao;
import com.amissible.dataMigration.dao.ProcessDao;
import com.amissible.dataMigration.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class SetupService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ConnectionDao connectionDao;

    @Autowired
    private ProcessDao processDao;

    public void setupAppDatabase(){
        userDao.createTable();
        connectionDao.createTable();
        processDao.createTable();
        new File("/app/dataMigration/data").mkdirs();
    }
}
