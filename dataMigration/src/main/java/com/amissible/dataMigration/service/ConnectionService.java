package com.amissible.dataMigration.service;

import com.amissible.dataMigration.dao.ConnectionDao;
import com.amissible.dataMigration.exception.AppException;
import com.amissible.dataMigration.model.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Service
public class ConnectionService {

    @Autowired
    private ConnectionDao connectionDao;

    @Autowired
    private Map<Long, JdbcTemplate> connectionMap;

    public List<Connection> getAllConnections(){
        return connectionDao.getAll();
    }

    public Connection getConnection(Long connectionId){
        return connectionDao.getById(connectionId);
    }

    public Connection saveConnection(Connection connection){
        Connection connection1 = connectionDao.getByName(connection.getName());
        if(connection1 != null)
            throw new AppException("Connection with same name is already available", HttpStatus.CONFLICT);
        connectionDao.save(connection);
        connection1 = connectionDao.getByName(connection.getName());
        if(connection1 == null)
            throw new AppException("Connection could not be saved", HttpStatus.INTERNAL_SERVER_ERROR);
        return connection1;
    }

    public void updateConnection(Connection connection){
        // on update, refresh template
        connectionDao.update(connection);
    }

    public void deleteConnection(Long connectionId){
        // Enhancement - Delete only if it is not being used
        connectionDao.delete(connectionId);
    }

    public List<String> getDatabases(Long connectionId){
        JdbcTemplate template = getConnectionJdbc(connectionId);
        List<String> schemaList = template.queryForList("SELECT schema_name FROM information_schema.schemata",String.class);
        return schemaList;
    }

    public List<String> getTables(Long connectionId,String schema){
        JdbcTemplate template = getConnectionJdbc(connectionId);
        List<String> tableList = template.queryForList("SELECT table_name FROM information_schema.tables WHERE table_schema='"+schema+"'",String.class);
        return tableList;
    }

    public List<String> getHeaders(Long connectionId,String schema,String table){
        JdbcTemplate template = getConnectionJdbc(connectionId);
        List<String> headers = template.queryForList("SELECT column_name FROM information_schema.columns WHERE table_schema='"+schema+"' AND table_name='"+table+"' ORDER BY ordinal_position ASC",String.class);
        return headers;
    }

    public JdbcTemplate getConnectionJdbc(Long connectionId){
        JdbcTemplate template = connectionMap.get(connectionId);
        if(template == null){
            Connection connection = getConnection(connectionId);
            if(connection == null)
                throw new AppException("Connection details not found",HttpStatus.NOT_FOUND);
            DataSource dataSource = DataSourceBuilder.create().username(connection.getUsername()).password(connection.getPassword()).url(connection.getUrl()).driverClassName(connection.getDriver()).build();
            template = new JdbcTemplate(dataSource);
            if(template == null)
                throw new AppException("Failed to get Connectivity",HttpStatus.INTERNAL_SERVER_ERROR);
            connectionMap.put(connectionId,template);
        }
        // Enhancement - Test Connection
        return template;
    }


}
