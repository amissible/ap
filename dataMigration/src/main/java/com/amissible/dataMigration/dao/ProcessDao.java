package com.amissible.dataMigration.dao;

import com.amissible.dataMigration.model.Process;

import java.util.Arrays;
import java.util.List;

public interface ProcessDao {
    void createTable();
    List<Process> getAll();
    Process getById(Long id);
    Process getByName(String name);
    void update(Process process);
    void save(Process process);
    void delete(Long id);
    void deleteAll();
    void dropTable();
}
