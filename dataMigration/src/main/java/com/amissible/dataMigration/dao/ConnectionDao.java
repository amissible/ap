package com.amissible.dataMigration.dao;

import com.amissible.dataMigration.model.Connection;

import java.util.List;

public interface ConnectionDao {
    void createTable();
    List<Connection> getAll();
    Connection getById(Long id);
    Connection getByName(String name);
    void update(Connection connection);
    void save(Connection connection);
    void delete(Long id);
    void deleteAll();
    void dropTable();
}
