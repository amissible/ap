package com.amissible.dataMigration.dao;

import com.amissible.dataMigration.model.User;

import java.util.List;

public interface UserDao {
    void createTable();
    List<User> getAll();
    User getById(Long id);
    User getByEmail(String email);
    void update(User user);
    void save(User user);
    void delete(Long id);
    void deleteByEmail(String email);
    void deleteAll();
    void dropTable();
}
