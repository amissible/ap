package com.amissible.dataMigration.config;

import com.amissible.dataMigration.model.ProcessJob;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Configuration
public class AppConfiguration {

    @Bean(name="connectionMap")
    public Map<Long, JdbcTemplate> getConnectionMap(){
        return new HashMap<Long,JdbcTemplate>();
    }

    @Bean(name="processMap")
    public Map<Long, ProcessJob> getProcessMap(){
        return new HashMap<Long,ProcessJob>();
    }

    @Bean(name="deletionQueue")
    public BlockingQueue<String> getDeletionQueue(){
        return new LinkedBlockingQueue<>();
    }
}
