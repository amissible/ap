package com.amissible.dataMigration.model;

public class Connection {
    private Long id;
    private String name;
    private String url;
    private String driver;
    private String username;
    private String password;

    public Connection() {
        id = 0L;
        name = url = driver = username = password = "";
    }

    public Connection(String name, String url, String driver, String username, String password) {
        this.name = name;
        this.url = url;
        this.driver = driver;
        this.username = username;
        this.password = password;
    }

    public Connection(Long id, String name, String url, String driver, String username, String password) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.driver = driver;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Connection{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", driver='" + driver + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
