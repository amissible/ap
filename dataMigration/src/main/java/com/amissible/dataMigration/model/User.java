package com.amissible.dataMigration.model;

public class User {

    private Long id;
    private String name;
    private String description;
    private String email;
    private String password;
    private String timeStamp;

    public User() {
        id = 0L;
        name = description = email = password = timeStamp = "";
    }

    public User(String name, String description, String email, String password, String timeStamp) {
        this.name = name;
        this.description = description;
        this.email = email;
        this.password = password;
        this.timeStamp = timeStamp;
    }

    public User(Long id, String name, String description, String email, String password, String timeStamp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.email = email;
        this.password = password;
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
