package com.amissible.dataMigration.model;

import com.amissible.dataMigration.exception.AppException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.ResultSetWrappingSqlRowSet;

import java.io.File;
import java.sql.Blob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProcessJob extends Thread{
    private Process process;
    //private DataReader dataReader;
    //private DataWriter dataWriter;
    private JdbcTemplate source;
    private JdbcTemplate destination;
    private Boolean exit;
    private Boolean pause;
    private Long lap;
    private Long records;
    private Long recordsIn;
    private Long recordsOut;
    private Long recordsErr;

    public ProcessJob(Process process,JdbcTemplate source,JdbcTemplate destination) {
        this.process = process;
        this.source = source;
        this.destination = destination;
        new File("data/process_"+process.getId()+"/csv").mkdirs();
        lap = records = recordsIn = recordsOut = recordsErr = 0L;
        exit = true;
        pause = false;
    }

    @Override
    public void run() {
        String sql,header = "",values;
        for(int i=0;i<process.getHeaders().length;i++){
            if(i==0)
                header += process.getHeaders()[i];
            else
                header += ","+process.getHeaders()[i];
        }
        while(!exit){
            try{
                while (pause)
                    Thread.sleep(1000*10);
                List<List<Object>> result = new ArrayList<>();
                sql = "SELECT * FROM "+process.getSourceSchema()+"."+process.getSourceTable()+" LIMIT "+recordsIn+","+(recordsIn+process.getPageSize());
                System.out.println(sql);
                source.query(sql,new Object[]{},(rs,rowNum) -> result.add(resultSetObject(rs)));
                System.out.println("Result Size = "+result.size());
                recordsIn += result.size();
                for(int row=0;row<result.size();row++){
                    sql = "INSERT INTO "+process.getDestinationSchema()+"."+process.getDestinationTable()+"("+header+") VALUES(";
                    for(int i=0;i<process.getHeaders().length;i++){
                        if(process.getTypes()[i].equals("TEXT"))
                            sql += (i==0?"":",") + "\'" + result.get(row).get(i) + "\'";
                        else if(process.getTypes()[i].equals("INTEGER"))
                            sql += (i==0?"":",") + result.get(row).get(i);
                        else if(process.getTypes()[i].equals("DECIMAL"))
                            sql += (i==0?"":",") + result.get(row).get(i);
                        else if(process.getTypes()[i].equals("BLOB"))
                            sql += (i==0?"":",") + result.get(row).get(i);
                    }
                    sql += ")";
                    System.out.println(sql);
                    destination.update(sql);
                    recordsOut++;
                }
                System.out.println(new Date());
                Thread.sleep(1000*10);
            }
            catch (Exception e){
                System.out.println("ERROR " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public List<Object> resultSetObject(ResultSet resultSet){
        List<Object> resultObjects = new ArrayList<>();
        try{
            for(int i=1;i<=process.getHeaders().length;i++) {
                if(process.getTypes()[i-1].equals("BLOB")){
                    Blob b = resultSet.getBlob(i);
                    resultObjects.add("0x"+encodeHexString(b.getBytes(1, (int)b.length())));
                }
                else
                    resultObjects.add(resultSet.getObject(i));
            }
        }
        catch (Exception e){
            System.out.println("PARSE ERROR : "+e.getMessage());
            e.printStackTrace();
        }
        return resultObjects;
    }

    public String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }

    public String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public Boolean initiate(){
        if(exit){
            exit = false;
            start();
            return true;
        }
        return false;
    }

    public Boolean terminate(){
        if(!exit) {
            exit = true;
            return true;
        }
        return false;
    }

    public void pause(){
        pause = true;
    }

    public void play(){
        pause = false;
    }
}
