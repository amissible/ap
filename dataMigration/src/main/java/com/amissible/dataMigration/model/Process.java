package com.amissible.dataMigration.model;

import java.util.Arrays;

public class Process {

    private Long id;
    private String name;
    private Long userId;
    private Long sourceConnectionId;
    private String sourceSchema;
    private String sourceTable;
    private Long destinationConnectionId;
    private String destinationSchema;
    private String destinationTable;
    private Long pageSize;
    private String timeStamp;
    private String headers[];
    private String types[];



    public Process(Long id, String name, Long userId, Long sourceConnectionId, String sourceSchema, String sourceTable, Long destinationConnectionId, String destinationSchema, String destinationTable, Long pageSize, String timeStamp, String[] headers, String[] types) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.sourceConnectionId = sourceConnectionId;
        this.sourceSchema = sourceSchema;
        this.sourceTable = sourceTable;
        this.destinationConnectionId = destinationConnectionId;
        this.destinationSchema = destinationSchema;
        this.destinationTable = destinationTable;
        this.pageSize = pageSize;
        this.timeStamp = timeStamp;
        this.headers = headers;
        this.types = types;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSourceConnectionId() {
        return sourceConnectionId;
    }

    public void setSourceConnectionId(Long sourceConnectionId) {
        this.sourceConnectionId = sourceConnectionId;
    }

    public String getSourceSchema() {
        return sourceSchema;
    }

    public void setSourceSchema(String sourceSchema) {
        this.sourceSchema = sourceSchema;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

    public Long getDestinationConnectionId() {
        return destinationConnectionId;
    }

    public void setDestinationConnectionId(Long destinationConnectionId) {
        this.destinationConnectionId = destinationConnectionId;
    }

    public String getDestinationSchema() {
        return destinationSchema;
    }

    public void setDestinationSchema(String destinationSchema) {
        this.destinationSchema = destinationSchema;
    }

    public String getDestinationTable() {
        return destinationTable;
    }

    public void setDestinationTable(String destinationTable) {
        this.destinationTable = destinationTable;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String[] getHeaders() {
        return headers;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "Process{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userId=" + userId +
                ", sourceConnectionId=" + sourceConnectionId +
                ", sourceSchema='" + sourceSchema + '\'' +
                ", sourceTable='" + sourceTable + '\'' +
                ", destinationConnectionId=" + destinationConnectionId +
                ", destinationSchema='" + destinationSchema + '\'' +
                ", destinationTable='" + destinationTable + '\'' +
                ", pageSize=" + pageSize +
                ", timeStamp='" + timeStamp + '\'' +
                ", headers=" + Arrays.toString(headers) +
                ", types=" + Arrays.toString(types) +
                '}';
    }
}
