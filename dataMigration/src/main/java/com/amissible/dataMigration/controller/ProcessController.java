package com.amissible.dataMigration.controller;

import com.amissible.dataMigration.model.Connection;
import com.amissible.dataMigration.model.Process;
import com.amissible.dataMigration.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/process")
public class ProcessController {

    @Autowired
    private ProcessService processService;

    @GetMapping("/getAllProcesses")
    public List<Process> getAllProcesses(){
        return processService.getAllProcesses();
    }

    @GetMapping("/getProcess")
    public Process getProcess(@RequestParam Long processId){
        return processService.getProcess(processId);
    }

    @PostMapping("/saveProcess")
    public Process saveProcess(@RequestBody Process process){
        return processService.saveProcess(process);
    }

    @DeleteMapping("/deleteProcess")
    public void deleteProcess(@RequestParam Long processId){
        processService.deleteProcess(processId);
    }

    @GetMapping("/startProcess")
    public Boolean startProcess(@RequestParam Long processId){
        return processService.startProcess(processId);
    }

    @GetMapping("/killProcess")
    public Boolean killProcess(@RequestParam Long processId){
        return processService.killProcess(processId);
    }

    @GetMapping("/pauseProcess")
    public void pauseProcess(@RequestParam Long processId){
        processService.pauseProcess(processId);
    }

    @GetMapping("/playProcess")
    public void playProcess(@RequestParam Long processId){
        processService.playProcess(processId);
    }

}
