package com.amissible.dataMigration.controller;

import com.amissible.dataMigration.model.Connection;
import com.amissible.dataMigration.service.ConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/connection")
public class ConnectionController {

    @Autowired
    private ConnectionService connectionService;

    @GetMapping("/getAllConnections")
    public List<Connection> getAllConnections(){
        return connectionService.getAllConnections();
    }

    @GetMapping("/getConnection")
    public Connection getConnection(@RequestParam Long connectionId){
        return connectionService.getConnection(connectionId);
    }

    @PostMapping("/saveConnection")
    public Connection saveConnection(@RequestBody Connection connection){
        return connectionService.saveConnection(connection);
    }

    @PostMapping("/updateConnection")
    public void updateConnection(@RequestBody Connection connection){
        connectionService.updateConnection(connection);
    }

    @DeleteMapping("/deleteConnection")
    public void deleteConnection(@RequestParam Long connectionId){
        connectionService.deleteConnection(connectionId);
    }

    @GetMapping("/getDatabases")
    public List<String> getDatabases(@RequestParam Long connectionId){
        return connectionService.getDatabases(connectionId);
    }

    @GetMapping("/getTables")
    public List<String> getTables(@RequestParam Long connectionId,@RequestParam String schema){
        return connectionService.getTables(connectionId,schema);
    }

    @GetMapping("/getHeaders")
    public List<String> getHeaders(@RequestParam Long connectionId,@RequestParam String schema,@RequestParam String table){
        return connectionService.getHeaders(connectionId,schema,table);
    }




/*
    @RequestMapping("/test")
    public void getConnectionJdbc(){
        connectionService.getConnectionJdbc(1L);
    }*/
}
