package com.amissible.dataMigration.controller;

import com.amissible.dataMigration.service.SetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/setup")
public class SetupController {

    @Autowired
    private SetupService setupService;

    @GetMapping("/database")
    public void setupAppDatabase(){
        setupService.setupAppDatabase();
    }
}
