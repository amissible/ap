import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop/shop/shop.component';
import { ProductComponent } from './product/product/product.component'

const routes: Routes = [
  { path: '', component: ShopComponent },
  { path: 'productsdetails', component: ProductComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
