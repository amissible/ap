import { Component, OnInit } from '@angular/core';
import { HttpproductsService, Product } from 'src/app/service/httpproducts.service';

@Component({
  selector: 'app-quickview',
  templateUrl: './quickview.component.html',
  styleUrls: ['./quickview.component.css']
})
export class QuickviewComponent implements OnInit {
  products : Product;

imgpath = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Blue-Bracelet-Canada.png";
imgpath2 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Grey-Bracelet-Canada.png";
imgpath3 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Peach-Bracelet-Canada.png";
imgpath4 = "https://shieldhands.com/wp-content/uploads/2020/10/Amazon-Product-Yellow-Bracelet-Canada.png";


  constructor(private httpproduct: HttpproductsService) { }

  ngOnInit(): void {
    this.httpproduct.getQuickProduct().subscribe(
      response =>{this.products = response;}
     );
  }

}
