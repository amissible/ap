import { Component, OnInit } from '@angular/core';
import { HttpproductsService, Product } from 'src/app/service/httpproducts.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {

  products : Product;

  constructor(private httpproduct: HttpproductsService) { }

  ngOnInit(): void {
    this.httpproduct.getProductDetails().subscribe(
      response =>{this.products = response;}
     );
  }

}
