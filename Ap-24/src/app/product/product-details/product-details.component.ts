import { Component, OnInit } from '@angular/core';
import { HttpproductsService, Product } from 'src/app/service/httpproducts.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  products : Product;

  constructor(private httpproduct: HttpproductsService) { }

  ngOnInit(): void {
    this.httpproduct.getProductDetails().subscribe(
      (response:any) =>{this.products = response;}
     );
  }

}
