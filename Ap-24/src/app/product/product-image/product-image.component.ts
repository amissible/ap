import { Component, OnInit } from '@angular/core';
import { HttpproductsService, Product } from 'src/app/service/httpproducts.service';

@Component({
  selector: 'app-product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['./product-image.component.css']
})
export class ProductImageComponent implements OnInit {

products : Product;

  constructor(private httpproduct: HttpproductsService) { }

  ngOnInit(): void {
    this.httpproduct.getProductDetails().subscribe(
      response =>{this.products = response;}
     );
  }

}
