import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface Product {
  category: any;
  product_name: any;
  alias : any;
  icon1_path : any;
  icon2_path : any;
  brand : any;
  image_path : any;
  info : any;
  attributes :any;
  stock_quantity: any;
}

@Injectable({
  providedIn: 'root'
})
export class HttpproductsService {

  constructor(private httpproduct: HttpClient) { }

  getProducts() {
    return this.httpproduct.get<Product>('http://www.amissible.com:8082/db/catalog');
  }

  getProductDetails() {
    return this.httpproduct.get<Product>('http://www.amissible.com:8082/db/product/blue-sanitizer-bracelet');
  }
  getQuickProduct() {
    return this.httpproduct.get<Product>('http://www.amissible.com:8082/db/quickview/grey-sanitizer-bracelet');
  }
}
