import { Component, OnInit } from '@angular/core';
import { HttpproductsService, Product } from 'src/app/service/httpproducts.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products : Product;

  constructor(private httpproduct: HttpproductsService) { }

  ngOnInit(): void {
    this.httpproduct.getProducts().subscribe(
      response =>{this.products = response;}
     );
  }

}
