import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MaterialModule} from './material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import { ProductsComponent } from './shop/products/products.component';
import { FilterPipe } from './pipservice/filter.pipe';
import { CategoryComponent } from './shop/category/category.component';
import { ShopComponent } from './shop/shop/shop.component';
import { SortingPipe } from './pipservice/sorting.pipe';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { ProductInfoComponent } from './product/product-info/product-info.component';
import { RelatedProductComponent } from './product/related-product/related-product.component';
import { ProductImageComponent } from './product/product-image/product-image.component';
import { ProductComponent } from './product/product/product.component';
import { QuickviewComponent } from './product/quickview/quickview.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navigation/navbar/navbar.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchComponent } from './shop/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
    CategoryComponent,
    ProductsComponent,
    FilterPipe,
    SortingPipe,
    ProductDetailsComponent,
    ProductInfoComponent,
    RelatedProductComponent,
    ProductImageComponent,
    ProductComponent,
    QuickviewComponent,
    NavbarComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FlexModule,
    MaterialModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
