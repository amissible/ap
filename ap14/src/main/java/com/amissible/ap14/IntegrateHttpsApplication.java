package com.amissible.ap14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrateHttpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegrateHttpsApplication.class, args);
	}

}
