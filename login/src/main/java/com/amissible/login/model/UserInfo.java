package com.amissible.login.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;

public class UserInfo extends User {

    private String password;

    //Default constructor for creating a user
    public UserInfo(String username, String password) {
        super(username, password, new ArrayList<>());
        this.password = password;
     }

    //Constructor for creating user with authorities
    public UserInfo(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.password = password;
    }

    //Constructor with additional stuff
    public UserInfo(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.password = password;
    }

    @Override
    public String getPassword() {
        return this.password;
    }
}
