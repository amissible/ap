package com.amissible.login.model;

import java.io.Serializable;

//Same as login bean
public class JwtRequest implements Serializable {
    private static final long serialVersionUID = 456789L;

    private String username;
    private String password;

    //default constructor for JSON parsing
    public JwtRequest()
    {

    }

    public JwtRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
