package com.amissible.login.model;

import java.io.Serializable;

//Creates response containing jwt to return to user
public class JwtResponse implements Serializable {
    private static final long serialVersionUID = 123789L;
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return this.jwttoken;
    }
}
