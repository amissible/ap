package com.amissible.login.dao;

import com.amissible.login.model.UserInfo;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserDaoImpl implements UserDao {
    //attributes

    Map<String, UserInfo> userMap = new HashMap<>();

    //hardcoded values to test multiple users, using in-memory storage
    UserDaoImpl()
    {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        UserInfo user1 =new UserInfo("Sparsh", passwordEncoder.encode("letMeIn"));
        UserInfo user2 =new UserInfo("Omendra", passwordEncoder.encode("letmein"));
        UserInfo user3 =new UserInfo("Umair", passwordEncoder.encode("letmeIn"));

        userMap.put("Sparsh", user1);
        userMap.put("Omendra", user2);
        userMap.put("Umair", user3);
    }

    @Override
    public Boolean register(UserInfo bean) {
        if(userMap.containsKey(bean.getUsername()))
           return false;
        userMap.put(bean.getUsername(), bean);
            return true;
    }

    @Override
    public Boolean findUser(String username) {
        if(userMap.containsKey(username))
            return true;
        else
            return false;
    }

    @Override
    public UserInfo getUserByUsername(String username) throws UsernameNotFoundException {
        if(findUser(username))
            return userMap.get(username);
        else
            throw new UsernameNotFoundException("User not found with username: " + username);
    }
}
