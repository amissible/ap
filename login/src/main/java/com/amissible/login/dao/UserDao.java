package com.amissible.login.dao;

import com.amissible.login.model.UserInfo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserDao {
    public Boolean register(UserInfo bean);
    public Boolean findUser(String username);
    public UserInfo getUserByUsername(String username) throws UsernameNotFoundException;
}
