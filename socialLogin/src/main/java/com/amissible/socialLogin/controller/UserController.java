package com.amissible.socialLogin.controller;

import com.amissible.socialLogin.model.GoogleResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @RequestMapping("/auth/response")
    public GoogleResponse response(@RequestParam String email,@RequestParam String name,@RequestParam String given_name,@RequestParam String family_name,@RequestParam String picture){
        GoogleResponse googleResponse = new GoogleResponse();
        googleResponse.setEmail(email);
        googleResponse.setName(name);
        googleResponse.setGiven_name(given_name);
        googleResponse.setFamily_name(family_name);
        googleResponse.setPicture(picture);
        return googleResponse;
    }
}
