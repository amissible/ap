package com.amissible.socialLogin.oauth2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (response.isCommitted()) {
            return;
        }
        DefaultOidcUser oidcUser = (DefaultOidcUser) authentication.getPrincipal();
        Map<String, Object> attributes = oidcUser.getAttributes();
        String email = (String) attributes.get("email");
        String name = (String) attributes.get("name");
        String given_name = (String) attributes.get("given_name");
        String family_name = (String) attributes.get("family_name");
        String picture = (String) attributes.get("picture");
        String redirectionUrl = UriComponentsBuilder.fromUriString("/auth/response?email="+email+"&name="+name+"&given_name="+given_name+"&family_name="+family_name+"&picture="+picture).build().toUriString();
        logger.info("GOOGLE VERIFIED FOR "+email);
        getRedirectStrategy().sendRedirect(request, response, redirectionUrl);
    }
}
