package com.amissible.TwoFactorTOTPAuth.controller;

import com.amissible.TwoFactorTOTPAuth.util.EmailService;
import com.amissible.TwoFactorTOTPAuth.util.TOTPService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping(value = "/TwoFactorTOTPAuth")
public class TwoFactorTotpAuthController {
    @Value("${exportDir}")
    String exportDir;

    @RequestMapping(value = "/getQR")
    public @ResponseBody ResponseEntity getQR(@RequestParam String secretKey, @RequestParam String account, @RequestParam String issuer){
        try{
            String path = exportDir+"/"+secretKey+".png";
            String barCodeUrl = TOTPService.getGoogleAuthenticatorBarCode(secretKey, account, issuer);
            TOTPService.createQRCode(barCodeUrl,path, 400, 400);
            Path filePath = Paths.get(path);
            if (Files.exists(filePath)) {
                EmailService emailService = new EmailService();
                emailService.sendEmail(account,null,null,"QR Authentication",
                        "Please scan the below QR code in Authenticator App",
                        new String[]{path});
                return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(Files.readAllBytes(filePath));
            }
            else
                throw new RuntimeException("File Not Found");
        }
        catch (Exception e){
            throw new RuntimeException("ERROR = "+e.getMessage());
        }
    }

    @RequestMapping(value = "/verifyTOTP")
    public @ResponseBody
    Boolean verifyTOTP(@RequestParam String secretKey,@RequestParam String code) {
        String otp = TOTPService.getTOTPCode(secretKey);
        if (code.equals(otp)) {
            return true;
        } else {
            return false;
        }
    }

}

