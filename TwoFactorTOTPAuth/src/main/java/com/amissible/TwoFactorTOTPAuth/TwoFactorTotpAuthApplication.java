package com.amissible.TwoFactorTOTPAuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwoFactorTotpAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwoFactorTotpAuthApplication.class, args);
	}

}
