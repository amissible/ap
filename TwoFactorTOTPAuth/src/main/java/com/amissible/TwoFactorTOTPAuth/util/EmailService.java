package com.amissible.TwoFactorTOTPAuth.util;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;

public class EmailService {

    private Properties properties;

    public EmailService() {
        properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("email", "msinghgkp738@gmail.com");
        properties.put("password", "Welcome*123");
    }

    public void setConfiguration(String host,String port,String email,String password){
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("email", email);
        properties.put("password", password);
    }

    public void sendEmail(String to,String cc,String bcc,String subject,String content,String files[]) {
        try{
            Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(properties.getProperty("email"),properties.getProperty("password"));
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(properties.getProperty("email"), false));
            if(to != null && to.length() > 0)
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            if(cc != null && cc.length() > 0)
                msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
            if(bcc != null && bcc.length() > 0)
                msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));

            msg.setSubject(subject);
            msg.setSentDate(new Date());

            Multipart multipart = new MimeMultipart();

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(content, "text/html");
            multipart.addBodyPart(messageBodyPart);

            if(files != null){
                for(int i=0;i<files.length;i++){
                    MimeBodyPart attachment = new MimeBodyPart();
                    attachment.attachFile(files[i]);
                    multipart.addBodyPart(attachment);
                }
            }

            msg.setContent(multipart);
            Transport.send(msg);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

}